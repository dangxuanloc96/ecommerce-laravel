<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Admin\AuthController;
use \App\Http\Controllers\Admin\HomeController;
use \App\Http\Controllers\Admin\UserController;
use \App\Http\Controllers\Admin\VariationController;
use \App\Http\Controllers\Admin\CategoryController;
use \App\Http\Controllers\Admin\SubCategoryController;
use \App\Http\Controllers\Admin\ProductController;
use \App\Http\Controllers\Admin\ProductVariationController;
use \App\Http\Controllers\Admin\ProductItemController;
use \App\Http\Controllers\Admin\ProductStockController;
use \App\Http\Controllers\Admin\ContactController;

Route::get('/login', [AuthController::class, 'getLogin'])->name('admin.get-login');
Route::post('/login', [AuthController::class, 'postLogin'])->name('admin.login');
Route::get('/logout', [AuthController::class, 'logout'])->name('admin.logout');

Route::group([
    'middleware' => ['admin'],
    'as' => 'admin::'
], function () {
    Route::middleware('admin')->group(function () {
        Route::get('/home', [HomeController::class, 'index'])->name('home');

        // management user
        Route::prefix('user')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('user.index');
            Route::put('/active/{id}', [UserController::class, 'activeUser'])->name('user.active');
            Route::delete('/delete/{id}', [UserController::class, 'destroy'])->name('user.delete');
        });

        // management variation
        Route::prefix('variation')->group(function () {
            Route::get('/', [VariationController::class, 'index'])->name('variation.index');
            Route::get('/create', [VariationController::class, 'create'])->name('variation.create');
            Route::post('/store', [VariationController::class, 'store'])->name('variation.store');
            Route::get('/edit/{id}', [VariationController::class, 'edit'])->name('variation.edit');
            Route::put('/update/{id}', [VariationController::class, 'update'])->name('variation.update');
            Route::delete('/delete/{id}', [VariationController::class, 'delete'])->name('variation.delete');
            Route::get('/{id}/options', [VariationController::class, 'listOption'])->name('variation.options');
        });

        // management categories
        Route::prefix('categories')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('category.index');
            Route::get('/create', [CategoryController::class, 'create'])->name('category.create');
            Route::post('/store', [CategoryController::class, 'store'])->name('category.store');
            Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
            Route::put('/store/{id}', [CategoryController::class, 'update'])->name('category.update');
            Route::delete('/delete/{id}', [CategoryController::class, 'delete'])->name('category.delete');
            Route::get('/{id}/sub-category', [SubCategoryController::class, 'getListSubByCategoryID'])->name('category.listSub');
            Route::get('/{id}/list-sub-category', [SubCategoryController::class, 'getListSubByCategoryIDView'])->name('category.listSub.view');
        });

        // management sub-categories
        Route::prefix('sub-categories')->group(function () {
            Route::get('/', [SubCategoryController::class, 'index'])->name('sub-category.index');
            Route::get('/create', [SubCategoryController::class, 'create'])->name('sub-category.create');
            Route::post('/store', [SubCategoryController::class, 'store'])->name('sub-category.store');
            Route::get('/edit/{id}', [SubCategoryController::class, 'edit'])->name('sub-category.edit');
            Route::put('/store/{id}', [SubCategoryController::class, 'update'])->name('sub-category.update');
            Route::delete('/delete/{id}', [SubCategoryController::class, 'delete'])->name('sub-category.delete');
        });

        // management products
        Route::prefix('products')->group(function () {
            Route::get('/', [ProductController::class, 'index'])->name('product.index');
            Route::get('/create', [ProductController::class, 'create'])->name('product.create');
            Route::post('/store', [ProductController::class, 'store'])->name('product.store');
            Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
            Route::put('/update/{id}', [ProductController::class, 'update'])->name('product.update');
            Route::delete('/delete/{id}', [ProductController::class, 'delete'])->name('product.delete');
        });

        // management product-variants
        Route::prefix('product-variations')->group(function () {
            Route::get('/product/{id}/list', [ProductVariationController::class, 'index'])->name('product-variation.index');
            Route::post('/product/{id}/store', [ProductVariationController::class, 'store'])->name('product-variation.store');
            Route::delete('/delete/{id}', [ProductVariationController::class, 'delete'])->name('product-variation.delete');
        });

        // management product-items
        Route::prefix('product-items')->group(function () {
            Route::get('/product/{id}/list', [ProductItemController::class, 'index'])->name('product-items.index');
            Route::post('/product/{id}/store', [ProductItemController::class, 'store'])->name('product-items.store');
            Route::put('/product/{productId}/update/{id}', [ProductItemController::class, 'update'])->name('product-items.update');
            Route::delete('/product/{productId}/delete/{id}', [ProductItemController::class, 'delete'])->name('product-items.delete');
        });

        // management product-stock
        Route::prefix('product-stocks')->group(function () {
            Route::post('/store', [ProductStockController::class, 'store'])->name('product-stocks.store');
            Route::get('/product-item/{id}/list', [ProductStockController::class, 'listStockByproductItemID'])->name('product-stocks.list');
        });

        // management contact
        Route::prefix('contacts')->group(function () {
            Route::get('/list', [ContactController::class, 'index'])->name('contact.index');
            Route::post('/{id}/reply', [ContactController::class, 'replyContact'])->name('contact.reply');
        });
    });
});

Route::fallback(function () {
    abort(404);
});


