<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\EndUser\AuthController;
use \App\Http\Controllers\EndUser\AddressController;
use \App\Http\Controllers\EndUser\ContactController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', [AuthController::class, 'getRegister'])->name('get-register');
Route::post('/register', [AuthController::class, 'postRegister'])->name('post-register');
Route::get('/register-complete/{id}',  [AuthController::class, 'getCompleteRegister'])->name('get-complete-register');
Route::get('/login', [AuthController::class, 'getLogin'])->name('get-login');
Route::post('/login', [AuthController::class, 'postLogin'])->name('post-login');
Route::put('/profile/{id}', [AuthController::class, 'updateProfile'])->name('update-profile');
Route::get('/profile/{id}', [AuthController::class, 'editProfile'])->name('edit-profile');
Route::get('/activeUser', [AuthController::class, 'activeUser'])->name('active-user');

//login by facebook
Route::get('/auth/redirect/facebook',  [AuthController::class,'redirectToFaceBook'])->name('login.facebook');
Route::get('/auth/facebook/callback',  [AuthController::class,'callbackFromFaceBook']);

//login by google
Route::get('/auth/redirect/google',  [AuthController::class,'redirectToGoogle'])->name('login.google');
Route::get('/auth/google/callback',  [AuthController::class,'callbackFromGoogle']);


Route::group(['prefix' => 'client'], function () {
    Route::get('home', function (){
        return view('end-users.home.index');
    })->name('home');

    Route::get('shop', function (){
        return view('end-users.shop.index');
    })->name('shop');

    Route::get('shop-detail', function (){
        return view('end-users.detail.index');
    })->name('detail');

    Route::get('list-cart', function (){
        return view('end-users.cart.index');
    })->name('list-cart');

    Route::get('checkout', function (){
        return view('end-users.checkout.index');
    })->name('checkout');

    Route::get('/contact', function (){
        return view('end-users.contact.index');
    })->name('contact');;

    Route::post('/contact', [ContactController::class, 'create'])->name('contact.create');;
});

Route::get('get-province', [AddressController::class, 'getListProvince']);
Route::get('get-district-follow-province/{id?}', [AddressController::class, 'getDistrictFollowProvince'])->name('get-district-follow-province');
