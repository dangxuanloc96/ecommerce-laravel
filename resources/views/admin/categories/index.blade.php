@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard v3</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @include('layouts.admin.elements.popup-notification-success')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Categories</h3>

                            <div class="card-tools">
                                <form action="{{ route('admin::category.index') }}" method="get" >
                                    <div class="input-group input-group-sm" style="width: 200px;">
                                        <a href="{{ route('admin::category.create') }}" style="margin-right: 5px" class="btn btn-primary btn-sm"><i class="fas fa-plus-square"></i> Create</a>
                                        <input type="text" name="key_search" class="form-control float-right"
                                               placeholder="Search" value="{{$keySearch}}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr style="text-align: center">
                                    <th>Name</th>
                                    <th>Category icon</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                @if($listCategory->count() > 0)
                                    @foreach($listCategory as $category)
                                        <tr>
                                            <td>{{$category->name}}</td>
                                            <td>
                                                <img src="{{asset("storage/$category->category_icon")}}" style="height: 5rem">
                                            </td>
                                            <td style="text-align: center">
                                                <a href="{{ route('admin::category.edit', $category->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                <form id='formDeleteModel_{{$category->id}}'
                                                      action="{{route('admin::category.delete', $category->id)}}" method="POST"
                                                      style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit" data-form-id="formDeleteModel_{{$category->id}}"
                                                            class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete">
                                                        <i class="fa-solid fas fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                                <div style="margin-top: 0.2rem">
                                                    <a href="{{route('admin::category.listSub.view', $category->id)}}" class="btn btn-success btn-sm">
                                                        <i class="fas fa-tshirt"></i>
                                                        Sub-category</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr style="text-align: center">
                                        <td colspan="3">Data empty</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
