@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Categories</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Edit</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <form  action="{{route('admin::category.update', $category->id)}}" method="post" enctype="multipart/form-data"
                       style="display: inline-block">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="card-body">
                        @if (!empty(session('error')))
                            <p class="text-red"><i class="fa fa-warning"></i> {{ session('error') }}</p>
                        @endif
                        <div class="row" style="margin-bottom: 20px">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Category name</label>
                                    <input type="text" class="form-control" name="name" value="{{$category->name}}">
                                    <div style="color: red"> {{ $errors->first('name') }} </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>Category icon</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="category_icon" value="">
                                    <div style="color: red"> {{ $errors->first('category_icon') }} </div>
                                    <label class="custom-file-label" id="customFileLabel" for="customFile">{{$category->category_icon}}</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <img class="img-account-profile mb-1" id="preview" name="image" style="margin-top: 10px; height: 10rem"
                                     src="{{asset("storage/$category->category_icon")}}"  alt="">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('add-script')
    <script>
        customFile.onchange = evt => {
            const [file] = customFile.files
            if (file) {
                console.log(file);
                preview.src = URL.createObjectURL(file)
                customFileLabel.innerHTML = file.name
            }
        }

    </script>
@endpush

