@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard v3</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    @include('layouts.admin.elements.popup-notification-success')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">USERS</h3>

                            <div class="card-tools">
                                <form action="{{ route('admin::user.index') }}" method="get">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="key_search" class="form-control float-right"
                                               placeholder="Search" value="{{$keySearch}}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr style="text-align: center">
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($listUser->count() > 0)
                                    @foreach($listUser as $key => $user)
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td style="text-align: center">{{$user->phone}}</td>
                                            <td style="text-align: center; color:  @if($user->status == USER_STATUS_UN_ACTIVE) red @else blue @endif"><b>{{USER_STATUS[$user->status]}}</b></td>
                                            <td style="text-align: center">
                                                @if($user->status == USER_STATUS_UN_ACTIVE)
                                                    <form action="{{route('admin::user.active', $user->id)}}" method="POST" style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PUT') }}
                                                        <button type="submit" class="btn btn-info btn-sm"><i class="fa-solid fas fa-check"></i> Active</button>
                                                    </form>
                                                @endif
                                                <form id = 'formDeleteModel_{{$user->id}}' action="{{route('admin::user.delete', $user->id)}}" method="POST" style="display: inline-block">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button type="submit" data-form-id="formDeleteModel_{{$user->id}}" class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete"><i class="fa-solid fas fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr style="text-align: center">
                                        <td colspan="6">Data empty</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
