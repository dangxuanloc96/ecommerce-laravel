@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Product</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @include('layouts.admin.elements.popup-notification-success')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title"><b>Add variation</b></h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <form action="{{route('admin::product-variation.store', $productID)}}" method="post"
                      enctype="multipart/form-data"
                      style="display: inline-block">
                    {{ csrf_field() }}
                    <div class="card-body">
                        @if (!empty(session('error')))
                            <p class="text-red"><i class="fa fa-warning"></i> {{ session('error') }}</p>
                        @endif
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 2rem">
                                <div class="form-group">
                                    <label>Variation name</label>
                                    <select class="form-control select2" style="width: 100%;" name="variation_id"
                                            id="variation_id">
                                        <option value="0" style="text-align: center"> --- Select ---</option>
                                        @foreach($listVariation as $variation)
                                            <option value="{{$variation->id}}">{{$variation->variation_name}}</option>
                                        @endforeach
                                    </select>
                                    <div style="color: red"> {{ $errors->first('variation_name') }} </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" id="variation-options">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
            <!-- /.row -->
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title"><b>List product variation</b></h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr style="text-align: center">
                            <th>Variation name</th>
                            <th>Variation value</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody style="text-align: center">
                        @if(count($listProductVariation) > 0)
                            @foreach($listProductVariation as $productVariation)
                                <tr>
                                    <td>{{$productVariation->variation_name}}</td>
                                    <td>
                                        {{implode("-",$productVariation->productVariationOptionValues->pluck('variation_name')->toArray())}}
                                    </td>
                                    <td style="text-align: center">
                                        <div>
                                            <form id='formDeleteModel_{{$productVariation->id}}'
                                                  action="{{route('admin::product-variation.delete', $productVariation->id)}}"
                                                  method="POST"
                                                  style="display: inline-block">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit"
                                                        data-form-id="formDeleteModel_{{$productVariation->id}}"
                                                        class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete">
                                                    <i class="fa-solid fas fa-trash"></i> Delete
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr style="text-align: center">
                                <td colspan="3">Data empty</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title"><b>List product sku</b></h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <form action="{{route('admin::product-items.store', $productID)}}" method="post" enctype="multipart/form-data">
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr style="text-align: center">
                                <th>
                                    #
                                </th>
                                <th>Sku</th>
                                <th>File</th>
                                <th>Image</th>
                                <th>
                                    <div class="checkbox select-all">
                                        <input id="all" type="checkbox"/>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody style="text-align: center">
                            @if(count($listProductVariationCombination) > 0)

                                {{ csrf_field() }}
                                @foreach($listProductVariationCombination as $key => $productVariationCombination)
                                    <tr>
                                        <td>
                                            {{$key + 1}}
                                        </td>
                                        <td>{{$productID.'-'.$productVariationCombination}}</td>
                                        <input style="display: none" name="combinations[]" value="{{$productID.'-'.$productVariationCombination}}"/>
                                        <td><input name="images[]" type="file" class="file-item"/></td>
                                        <td><img src="" style="width: 10rem"></td>
                                        <td>
                                            <div class="checkbox rows">
                                                <input id="box{{$key}}" onchange="setValue(this)" type="checkbox" name="items[]"/>
                                                <input style="display: none" name ='items_index[]' class="items_index" value="false">
                                                <label for="box{{$key}}"></label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr style="text-align: center">
                                    <td colspan="8">Data empty</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div style="margin-top: 0.5rem;  display: flex; justify-content: center;">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus-square"></i> Create
                            </button>
                            <button type="button" style="margin-left: 0.3rem" class="btn btn-info" id="refresh"><i class="fas fa-sync-alt"></i> Refresh
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('add-script')
    <script>
        $('#variation_id').change(function () {
            let variation_id = $(this).val();
            if (variation_id !== 0) {
                $.ajax({
                    url: `/admin/variation/${variation_id}/options`,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        let textHtmlOption = `<label>Variation value</label>` +
                            `<div class="form-group clearfix">`;

                        if (data.length == 0) {
                            textHtmlOption = ""
                        }

                        $.each(data, function (index) {
                            textHtmlOption += `<div class="icheck-primary d-inline">` +
                                `<input type="checkbox" id="checkboxPrimary${index}" value="${data[index].id}" name="variation_option_ids[]">` +
                                `<label for="checkboxPrimary${index}"> ${data[index].variation_value} </label> ` +
                                `</div>`
                        });

                        textHtmlOption += `</div>`;
                        $('#variation-options').html(textHtmlOption);
                    }
                });
            }
        });


        function setValue(e) {
            if ($(e).is(":checked")) {
                $(e).siblings('.items_index').val("true");
            } else {
                $(e).siblings('.items_index').val("false");
            }
        }

        $('#all').change(function(e) {
            if (e.currentTarget.checked) {
                $('.rows').find('input[type="checkbox"]').prop('checked', true);
                $('.items_index').val("true");
            } else {
                $('.rows').find('input[type="checkbox"]').prop('checked', false);
                $('.items_index').val("false");
            }
        });

        const inputs = document.querySelectorAll('.file-item');
        inputs.forEach((input) => {
            input.onchange = evt => {
                const [file] = input.files
                if (file) {
                    input.closest('tr').querySelectorAll('td img')[0].src = URL.createObjectURL(file)
                }
            }
        })

        $('#refresh').click(function () {
            location.reload(true);
        });

    </script>
@endpush

