@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard v3</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
   @include('layouts.admin.elements.popup-notification-success')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Products</h3>

                            <div class="card-tools">
                                <form action="{{ route('admin::product.index') }}" method="get">
                                    <div class="input-group input-group-sm" style="width: 200px;">
                                        <a href="{{ route('admin::product.create') }}" style="margin-right: 5px" class="btn btn-primary btn-sm"><i class="fas fa-plus-square"></i> Create</a>
                                        <input type="text" name="key_search" class="form-control float-right"
                                               placeholder="Search" value="{{$keySearch}}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr style="text-align: center">
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>SubCategory</th>
                                    <th>Promotion</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                @if($listProduct->count() > 0)
                                    @foreach($listProduct as $key => $product)
                                        <tr>
                                            <td>{{$key + 1}}</td>
                                            <td>{{$product->name}}</td>
                                            <td>
                                                {!! $product->description !!}
                                            </td>
                                            <td>
                                               <img src="{{asset("storage/$product->image")}}" style="width: 10rem">
                                            </td>
                                            <td>
                                               {{$product->category->name}}
                                            </td>
                                            <td>
                                               {{$product->subCategory->name}}
                                            </td>
                                            <td>
                                            </td>
                                            <td style="text-align: center">
                                                <div>
                                                    <a href="{{ route('admin::product.edit', $product->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                                    <form id='formDeleteModel_{{$product->id}}'
                                                          action="{{route('admin::product.delete', $product->id)}}" method="POST"
                                                          style="display: inline-block">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        <button type="submit" data-form-id="formDeleteModel_{{$product->id}}"
                                                                class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete">
                                                            <i class="fa-solid fas fa-trash"></i> Delete
                                                        </button>
                                                    </form>
                                                </div>
                                                <div style="margin-top: 0.2rem">
                                                    <a href="{{route('admin::product-variation.index', $product->id)}}" class="btn btn-warning btn-sm">
                                                        <i class="fa-sharp fas fa-light fa-star"></i>
                                                        Product variant</a>
                                                </div>
                                                <div style="margin-top: 0.2rem">
                                                    <a href="{{route('admin::product-items.index', $product->id)}}" class="btn btn-success btn-sm">
                                                        <i class="fas fa-tshirt"></i>
                                                        Product items</a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr style="text-align: center">
                                        <td colspan="8">Data empty</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
