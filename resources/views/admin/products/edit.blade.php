@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Product</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Edit</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <form  action="{{route('admin::product.update', $product->id)}}" method="post" enctype="multipart/form-data"
                       style="display: inline-block">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="card-body">
                        @if (!empty(session('error')))
                            <p class="text-red"><i class="fa fa-warning"></i> {{ session('error') }}</p>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" value="{{old('name') ?? $product->name}}">
                                    <div style="color: red"> {{ $errors->first('name') }} </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea id="summernote" name="description" class="form-control">
                                       {{old('description') ?? $product->description}}
                                    </textarea>
                                    <div style="color: red"> {{ $errors->first('description') }} </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label>image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image" value="">
                                    <label class="custom-file-label" id="customFileLabel" for="customFile">{{old('description') ?? $product->image}}</label>
                                    <div style="color: red"> {{ $errors->first('image') }} </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <img class="img-account-profile mb-1" id="preview" name="image" style="margin-top: 10px; height: 10rem"
                                     src="{{asset("storage/$product->image")}}"  alt="">
                            </div>
                            <div class="col-md-12" style="margin-top: 2rem">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control select2" style="width: 100%;" name="category_id" id="category">
                                        <option value="0" style="text-align: center"> --- Select ---</option>
                                        @foreach($listCategory as $category)
                                            @if($product->category_id == $category->id)
                                                <option value="{{$category->id}}"
                                                        selected="selected">{{$category->name}}</option>
                                            @else
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <div style="color: red"> {{ $errors->first('category_id') }} </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Sub category</label>
                                    <select class="form-control select2" style="width: 100%;" name="sub_category_id" id="sub_category_id">
                                        <option value="0" style="text-align: center"> --- Select ---</option>
                                        @foreach($listSubCategory as $subCategory)
                                            @if($product->sub_category_id == $subCategory->id)
                                                <option value="{{$subCategory->id}}"
                                                        selected="selected">{{$subCategory->name}}</option>
                                            @else
                                                <option value="{{$subCategory->id}}">{{$subCategory->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <div style="color: red"> {{ $errors->first('sub_category_id') }} </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('add-script')
    <script>
        customFile.onchange = evt => {
            const [file] = customFile.files
            if (file) {
                console.log(file);
                preview.src = URL.createObjectURL(file)
                customFileLabel.innerHTML = file.name
            }
        }


        $('#category').change(function () {
            let categoryID = $(this).val();
            if(categoryID !== 0 ) {
                $.ajax({
                    url: `/admin/categories/${categoryID}/sub-category`,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        let textHtmlOption = `<option value="0" style="text-align: center"> --- Select ---</option>`;
                        $.each(data, function(index) {
                            textHtmlOption += `<option value="${data[index].id}" >${data[index].name} </option>`
                        });
                        $('#sub_category_id').html(textHtmlOption);
                    }
                });
            }
        });

    </script>
@endpush
