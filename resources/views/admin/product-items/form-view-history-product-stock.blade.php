<!-- Delete Confirm - Helper. Show a modal box -->
<div class="modal fade" id="modalViewHistoryProductStock">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">History product stock</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title" id="title-view-history">Responsive Hover Table</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Unit price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Create date</th>
                                </tr>
                                </thead>
                                <tbody id="data-history-stock">
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" id="btnModalCancelModelViewHistoryProductStock" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Delete Confirm - Helper. Show a modal box -->

{{-- Initiate Confirm Delete --}}
<script type="text/javascript">

    function formatDateTime(objectDate) {
        let day = objectDate.getDate();
        let month = objectDate.getMonth();
        let year = objectDate.getFullYear();
        return day + "-" + month + "-" + year;
    }
    function viewHistoryStock(productItemId){
        $.ajax({
            url: `/admin/product-stocks/product-item/${productItemId}/list`,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                if(data == 0) {
                    alert('Update price Error!')
                } else {
                    let dataHistoryStock = "";
                   data.forEach(function (value, index) {
                     dataHistoryStock += `<tr> <td> ${index + 1} </td> <td> ${value.unit_price} </td> <td> ${value.total_stock} </td><td> ${value.total_price} </td> <td> ${value.created_at} </td> </tr>`
                   });

                   $('#data-history-stock').html(dataHistoryStock);
                }
            }

        });
    };

    $(function () {
        $(document).on('click', '.btnOpenerModalViewHistoryProductStock', function (e) {
            e.preventDefault();
            var formId = $(this).attr('data-product-item-id');
            var sku = $(this).attr('data-product-sku');
            $('#title-view-history').html(sku);
            viewHistoryStock(formId);
            $('#modalViewHistoryProductStock').modal('show');
        });
        // Modal Button Cancel Delete
        $(document).on('click', '#btnModalCancelModelViewHistoryProductStock', function (e) {
            e.preventDefault();
            $('#data-history-stock').html("");
            $('#modalConfirmModelCreateProductStock').modal('hide');
        });
    });
</script>
