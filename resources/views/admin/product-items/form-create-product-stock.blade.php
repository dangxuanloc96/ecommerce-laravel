<!-- Delete Confirm - Helper. Show a modal box -->
<div class="modal fade" id="modalConfirmModelCreateProductStock">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="text" class="form-control" id="pro_quantity" name="quantity" value="">
                        <div style="color: red">  </div>
                    </div>
                    <div class="form-group">
                        <label>Unit price</label>
                        <input type="text" class="form-control" id="pro_unit_price" name="unit_price" value="">
                        <div style="color: red">  </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" id="btnModalCancelModelCreateProductStock" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnModalConfirmModelCreateProductStock" data-form-id="">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Delete Confirm - Helper. Show a modal box -->

{{-- Initiate Confirm Delete --}}
<script type="text/javascript">
    function addStock(unitPrice, quantity, productItemId){
        $.ajax({
            url: `/admin/product-stocks/store`,
            type: 'POST',
            dataType: 'JSON',
            data: {unit_price: unitPrice, quantity, product_item_id : productItemId},
            success: function (data) {
                if(data == 0) {
                    alert('Update price Error!')
                } else {
                    $(`#available_stock_${productItemId}`).html(data.available_stock);
                    $(`#total_stock_${productItemId}`).html(data.total_stock);
                    $(`#total_price_${productItemId}`).html(data.total_price);
                }
            }

        });
    };

    $(function () {
        $(document).on('click', '.btnOpenerModalCreateProductStock', function (e) {
            e.preventDefault();
            var formId = $(this).attr('data-product-item-id');
            var btnConfirm = $('#modalConfirmModelCreateProductStock').find('#btnModalConfirmModelCreateProductStock');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', formId);
            }

            $('#modalConfirmModelCreateProductStock .modal-title').html($(this).attr('data-product-sku'));
            $('#modalConfirmModelCreateProductStock').modal('show');
        });
        // Modal Button Confirm
        $(document).on('click', '#btnModalConfirmModelCreateProductStock', function (e) {
            e.preventDefault();
            var quantity = $('#pro_quantity').val();
            var unitPrice = $('#pro_unit_price').val();
            var productItemId = $(this).attr('data-form-id');
            addStock(unitPrice, quantity, productItemId);
            $('#modalConfirmModelCreateProductStock').modal('hide');
            $('#pro_quantity').val("");
            $('#pro_unit_price').val("");
        });
        // Modal Button Cancel Delete
        $(document).on('click', '#btnModalConfirmModelCreateProductStock #btnModalCancelModelCreateProductStock', function (e) {
            e.preventDefault();
            var btnConfirm = $('#modalConfirmModelCreateProductStock').find('#btnModalConfirmModelCreateProductStock');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', "");
            }
            $('#modalConfirmModelCreateProductStock').modal('hide');
        });
    });
</script>
