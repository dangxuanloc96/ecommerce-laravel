@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard v3</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @include('layouts.admin.elements.popup-notification-success')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title"><b>List product item</b></h3>

                    <div class="card-tools">
                        <form action="{{ route('admin::product-items.index', $productID) }}" method="get">
                            <div class="input-group input-group-sm" style="width: 200px;">
                                <input type="text" name="key_search" class="form-control float-right"
                                       placeholder="Search" value="{{$keySearch}}">

                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr style="text-align: center">
                            <th>#</th>
                            <th>Image</th>
                            <th>Sku</th>
                            <th>Price</th>
                            <th>Available stock</th>
                            <th>Total stock</th>
                            <th>Total price stock</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody style="text-align: center">
                        @if(count($listProductItems) > 0)
                            @foreach($listProductItems as $key => $productItem)
                                <tr>
                                    <td>
                                        {{$key + 1}}
                                        <input type="text" style="display: none" name="product_item_ids[]"
                                               value="{{$productItem->id}}"/>
                                    </td>
                                    <td>
                                        <img src="{{asset("storage/$productItem->image")}}" style="width: 10rem">
                                    </td>
                                    <td>{{$productItem->sku}}</td>
                                    <td>
                                        <input type="number" name="prices[]" value="{{$productItem->price ?? 0}}"  min="0" data-item-id="{{$productItem->id}}"
                                               onkeyup="setPositivePrice(this)" onblur="updatePrice(this)"/>
                                    </td>
                                    <td id="available_stock_{{$productItem->id}}">
                                        {{$productItem->available_stock ?? 0}}
                                    </td>
                                    <td id="total_stock_{{$productItem->id}}">
                                        {{$productItem->productStocks->sum('total_stock')}}
                                    </td>
                                    <td id="total_price_{{$productItem->id}}">
                                        {{displayMoney($productItem->productStocks->sum('total_price'))}}
                                    </td>
                                    <td style="text-align: center">
                                        <div>
                                            <button type="button"
                                                    data-form-id="formCreateProductStock_{{$productItem->id}}" data-product-sku="{{$productItem->sku}}" data-product-item-id="{{$productItem->id}}"
                                                    class="btn btn-primary btn-sm btnOpenerModalCreateProductStock">
                                                <i class="fas fa-plus-square"></i> Add stock
                                            </button>
                                            <br>
                                            <button style="margin-top: 0.2rem" type="button"
                                                    data-form-id="formViewHistoryProductStock_{{$productItem->id}}" data-product-item-id="{{$productItem->id}}" data-product-sku = "{{$productItem->sku}}"
                                                    class="btn btn-warning btn-sm btnOpenerModalViewHistoryProductStock">
                                                <i class="fas fa-history"></i> History stock
                                            </button>
                                            <br>
                                            <form id='formDeleteModel_{{$productItem->id}}'
                                                  action="{{route('admin::product-items.delete', [$productID, $productItem->id])}}"
                                                  method="POST"
                                                  style="display: inline-block">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="button" style="margin-top: 0.2rem"
                                                        data-form-id="formDeleteModel_{{$productItem->id}}"
                                                        class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete">
                                                    <i class="fa-solid fas fa-trash"></i> Delete
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr style="text-align: center">
                                <td colspan="8">Data empty</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@push('add-script')
    <script>
        $('#variation_id').change(function () {
            let variation_id = $(this).val();
            if (variation_id !== 0) {
                $.ajax({
                    url: `/admin/variation/${variation_id}/options`,
                    type: 'GET',
                    dataType: 'JSON',
                    success: function (data) {
                        let textHtmlOption = `<label>Variation value</label>` +
                            `<div class="form-group clearfix">`;

                        if (data.length == 0) {
                            textHtmlOption = ""
                        }

                        $.each(data, function (index) {
                            textHtmlOption += `<div class="icheck-primary d-inline">` +
                                `<input type="checkbox" id="checkboxPrimary${index}" value="${data[index].id}" name="variation_option_ids[]">` +
                                `<label for="checkboxPrimary${index}"> ${data[index].variation_value} </label> ` +
                                `</div>`
                        });

                        textHtmlOption += `</div>`;
                        $('#variation-options').html(textHtmlOption);
                    }
                });
            }
        });


        function setValue(e) {
            if ($(e).is(":checked")) {
                $(e).siblings('.items_index').val("true");
            } else {
                $(e).siblings('.items_index').val("false");
            }
        }

        $('#all').change(function (e) {
            if (e.currentTarget.checked) {
                $('.rows').find('input[type="checkbox"]').prop('checked', true);
            } else {
                $('.rows').find('input[type="checkbox"]').prop('checked', false);
            }
        });

        const inputs = document.querySelectorAll('.file-item');
        inputs.forEach((input) => {
            input.onchange = evt => {
                const [file] = input.files
                if (file) {
                    input.closest('tr').querySelectorAll('td img')[0].src = URL.createObjectURL(file)
                }
            }
        })

        $('#refresh').click(function () {
            location.reload(true);
        });

        function setPositivePrice(e) {
            if($(e).val() < 0){$(e).val($(e).val() * -1)}
        }

        function updatePrice(e) {
            let price = $(e).val()
            let productId = {{$productID}};
            let productItemId = $(e).attr('data-item-id');
            $.ajax({
                url: `/admin/product-items/product/${productId}/update/${productItemId}`,
                type: 'PUT',
                dataType: 'JSON',
                data: {price: price},
                success: function (data) {
                    console.log(data);
                    if(data == 0) {
                        alert('Update price Error!')
                    }
                }

            });
        }

    </script>
@endpush

