@extends('layouts.admin-app')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard v3</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v3</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    @include('layouts.admin.elements.popup-notification-success')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Categories</h3>

                            <div class="card-tools">
                                <form action="{{ route('admin::contact.index') }}" method="get" >
                                    <div class="input-group input-group-sm" style="width: 200px;">
                                        <input type="text" name="key_search" class="form-control float-right"
                                               placeholder="Search" value="{{$keySearch}}">

                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr style="text-align: center">
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Feedback</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody style="text-align: center">
                                @if($listContact->count() > 0)
                                    @foreach($listContact as $contact)
                                        <tr>
                                            <td>{{$contact->name}}</td>
                                            <td>
                                                {{$contact->email}}
                                            </td>
                                            <td>
                                                {{$contact->subject}}
                                            </td>
                                            <td>
                                                {{$contact->message}}
                                            </td>
                                            <td id="status-contact-{{$contact->id}}">
                                                {!! ($contact->status == FEEDBACK) ? "<span style='color: blue'>done</span>" : "<span style='color:red'>Pending</span>" !!}
                                            </td>
                                            <td style="text-align: center" id="action-{{$contact->id}}">
                                                @if($contact->status == UN_FEEDBACK)
                                                    <button type="button"
                                                            data-message="{{$contact->message}}"
                                                            data-contact-id="{{$contact->id}}"
                                                            data-form-id="formCreateFeedback-{{$contact->id}}"
                                                            class="btn btn-primary btn-sm btnOpenerModalFeedbackContact">
                                                        <i class="fas fa-reply"></i> Reply
                                                    </button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr style="text-align: center">
                                        <td colspan="5">Data empty</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
