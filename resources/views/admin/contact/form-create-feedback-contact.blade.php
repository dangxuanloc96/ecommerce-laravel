<!-- Delete Confirm - Helper. Show a modal box -->
<div class="modal fade" id="modalCreateReplyContact">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Feedback</label>
                        <textarea class="form-control" name="feedback" id="feedback" disabled>

                        </textarea>
                    </div>
                    <div class="form-group">
                        <label>Reply</label>
                        <textarea class="form-control" name="reply" id="reply">

                        </textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" id="btnModalCancelCreateReply" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btnModalConfirmModelCreateReply" data-form-id="">Reply</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Delete Confirm - Helper. Show a modal box -->

{{-- Initiate Confirm Delete --}}
<script type="text/javascript">
    function replyContact(id, content){
        $.ajax({
            url: `/admin/contacts/${id}/reply`,
            type: 'POST',
            dataType: 'JSON',
            data: {reply: content},
            success: function (data) {
                if(data == 0) {
                    alert('reply Error!')
                } else {
                }
            }

        });
    };

    $(function () {
        $(document).on('click', '.btnOpenerModalFeedbackContact', function (e) {
            e.preventDefault();
            var formId = $(this).attr('data-contact-id');
            var btnConfirm = $('#modalCreateReplyContact').find('#btnModalConfirmModelCreateReply');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', formId);
            }

            $('#feedback').val($(this).attr('data-message'))

            $('#modalCreateReplyContact').modal('show');
        });
        // Modal Button Confirm
        $(document).on('click', '#btnModalConfirmModelCreateReply', function (e) {
            e.preventDefault();
            var contactId = $(this).attr('data-form-id');
            var reply = $('#reply').val();
            $(`#status-contact-${contactId}`).html("<span style='color: blue'>done</span>");
            $(`#action-${contactId}`).html("");
            replyContact(contactId, reply);
            $('#modalCreateReplyContact').modal('hide');
        });
        // Modal Button Cancel Delete
        $(document).on('click', '#modalCreateReplyContact #btnModalCancelCreateReply', function (e) {
            e.preventDefault();
            var btnConfirm = $('#modalCreateReplyContact').find('#btnModalCancelCreateReply');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', "");
            }
            $('#modalCreateReplyContact').modal('hide');
        });
    });
</script>
