<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>EShopper - Bootstrap Shop Template</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">

    <!-- Favicon -->
    <link href="{{asset('end-users/img/favicon.ico')}}" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap"
          rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="{{asset('end-users/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{asset('end-users/css/style.css')}}" rel="stylesheet">
    <style>
        body {
            background-color: #f2f6fc;
            color: #69707a;
        }

        .img-account-profile {
            height: 10rem;
            width: 10rem;
        }

        .rounded-circle {
            border-radius: 50% !important;
        }

        .card {
            box-shadow: 0 0.15rem 1.75rem 0 rgb(33 40 50 / 15%);
        }

        .card .card-header {
            font-weight: 500;
        }

        .card-header:first-child {
            border-radius: 0.35rem 0.35rem 0 0;
        }

        .card-header {
            padding: 1rem 1.35rem;
            margin-bottom: 0;
            background-color: rgba(33, 40, 50, 0.03);
            border-bottom: 1px solid rgba(33, 40, 50, 0.125);
        }

        .form-control, .dataTable-input {
            display: block;
            width: 100%;
            padding: 0.875rem 1.125rem;
            font-size: 0.875rem;
            font-weight: 400;
            line-height: 2;
            color: #69707a;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #c5ccd6;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: 0.35rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .nav-borders .nav-link.active {
            color: #0061f2;
            border-bottom-color: #0061f2;
        }

        .nav-borders .nav-link {
            color: #69707a;
            border-bottom-width: 0.125rem;
            border-bottom-style: solid;
            border-bottom-color: transparent;
            padding-top: 0.5rem;
            padding-bottom: 0.5rem;
            padding-left: 0;
            padding-right: 0;
            margin-left: 1rem;
            margin-right: 1rem;
        }

        .form-control-custom {
            padding: 0px !important;
        }
    </style>
</head>

<body>
<!-- Topbar Start -->
<div class="container-fluid">
    <div class="row bg-secondary py-2 px-xl-5">
        <div class="col-lg-6 d-none d-lg-block">
            <div class="d-inline-flex align-items-center">
                <a class="text-dark" href="">FAQs</a>
                <span class="text-muted px-2">|</span>
                <a class="text-dark" href="">Help</a>
                <span class="text-muted px-2">|</span>
                <a class="text-dark" href="">Support</a>
            </div>
        </div>
        <div class="col-lg-6 text-center text-lg-right">
            <div class="d-inline-flex align-items-center">
                <a class="text-dark px-2" href="">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-twitter"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-linkedin-in"></i>
                </a>
                <a class="text-dark px-2" href="">
                    <i class="fab fa-instagram"></i>
                </a>
                <a class="text-dark pl-2" href="">
                    <i class="fab fa-youtube"></i>
                </a>
            </div>
        </div>
    </div>
    <div class="row align-items-center py-3 px-xl-5">
        <div class="col-lg-3 d-none d-lg-block">
            <a href="" class="text-decoration-none">
                <h1 class="m-0 display-5 font-weight-semi-bold"><span
                        class="text-primary font-weight-bold border px-3 mr-1">E</span>Shopper</h1>
            </a>
        </div>
        <div class="col-lg-6 col-6 text-left">
        </div>
        <div class="col-lg-3 col-6 text-right">
            <a href="" class="btn border">
                <i class="fas fa-heart text-primary"></i>
                <span class="badge">0</span>
            </a>
            <a href="" class="btn border">
                <i class="fas fa-shopping-cart text-primary"></i>
                <span class="badge">0</span>
            </a>
        </div>
    </div>

    <div class="row px-xl-5">
        <!-- Shop Sidebar Start -->
        <div class="container-xl px-4 mt-4">
            <!-- Account page navigation-->
            <nav class="nav nav-borders">
                <a href="{{route('home')}}" class="nav-item nav-link active">Home</a>
                <a href="{{route('shop')}}" class="nav-item nav-link">Shop</a>
                <a href="{{route('detail')}}" class="nav-item nav-link">Shop Detail</a>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Pages</a>
                    <div class="dropdown-menu rounded-0 m-0">
                        <a href="{{route('list-cart')}}" class="dropdown-item">Shopping Cart</a>
                        <a href="{{route('checkout')}}" class="dropdown-item">Checkout</a>
                    </div>
                </div>
                <a href="{{route('contact')}}" class="nav-item nav-link">Contact</a>
                <a  style="margin-left: auto" href="{{route('edit-profile',$user->id)}}" class="nav-item nav-link"><i class="fa-solid fas fa-user"></i> {{$user->name}}</a>
            </nav>
            <hr class="mt-0 mb-4">
            <form action="{{route('update-profile', $user->id)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
            <div class="row">
                <div class="col-xl-4">
                    <!-- Profile picture card-->
                    <div class="card mb-4 mb-xl-0">
                        <div class="card-header">Profile Picture</div>
                        <div class="card-body text-center">
                            <!-- Profile picture image-->
                            <img class="img-account-profile rounded-circle mb-2" name="image"
                                 src="@if($user->image) {{asset("storage/$user->image")}} @else http://bootdey.com/img/Content/avatar/avatar1.png @endif"  alt="">
                            <!-- Profile picture help block-->
                            <div class="small font-italic text-muted mb-4">JPG or PNG no larger than 5 MB</div>
                            <!-- Profile picture upload button-->
                            <input type="file" class="btn btn-primary" id="choose-image" name="image" style="display: none" value="{{$user->image}}">
                            <button class="btn btn-primary" type="button" id="upload-image">Upload new image</button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8">
                    <!-- Account details card-->
                    <div class="card mb-4">
                        <div class="card-header">Account Details</div>
                        <div class="card-body">
                            <form>
                                <!-- Form Group (username)-->
                                <div class="mb-3">
                                    <label class="small mb-1" for="inputUsername">Name</label>
                                    <input class="form-control" id="inputUsername" type="text" name="name"
                                           placeholder="Enter your username" value="{{$user->name}}">
                                </div>
                                <!-- Form Row-->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (first name)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="inputFirstName">Email</label>
                                        <input class="form-control" id="inputFirstName" type="text" name="email"
                                               placeholder="Enter your email" value="{{$user->email}}">
                                    </div>
                                    <!-- Form Group (last name)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="inputLastName">Phone</label>
                                        <input class="form-control" id="inputLastName" type="text" name="phone"
                                               placeholder="Enter your phone" value="{{$user->phone}}">
                                    </div>
                                </div>
                                <div class="card-header" style="padding: 0.3rem !important;">Address 1 (default)</div>
                                <!-- Form Row        -->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (organization name)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="inputOrgName">Province</label>
                                        <select name="province_1" class="form-control form-control-custom" id="province_1">
                                            <option value="0" style="text-align: center"> --- Select ---</option>
                                            @foreach($listProvince as $province)
                                                @if(count($address) > 0 && $address[0]['province_id'] == $province['code'])
                                                    <option value="{{$province['code']}}" selected>{{$province['name']}}</option>
                                                @else
                                                    <option value="{{$province['code']}}">{{$province['name']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- Form Group (location)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="inputOrgName">District</label>
                                        <select name="district_1" class="form-control form-control-custom" id="district_1">
                                            <option value="0" style="text-align: center"> --- Select ---</option>
                                            @if(count($address) >= 1)
                                                @foreach($address[0]['district-relative'] as $district)
                                                    @if($district['code'] == $address[0]['district_id'])
                                                        <option value="{{$district['code']}}"
                                                                selected>{{$district['name']}}</option>
                                                    @else
                                                        <option value="{{$district['code']}}"
                                                        >{{$district['name']}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!-- Form Group (email address)-->
                                <div class="mb-3">
                                    <label class="small mb-1" for="inputEmailAddress">Detail</label>
                                    <input class="form-control" id="inputEmailAddress" type="text" name="address_1"
                                           placeholder="Enter your detail" value="@if(count($address) > 0) {{$address[0]['address_line_1']}} @endif">
                                </div>
                                <!-- Form Row-->
                                <div class="card-header" style="padding: 0.3rem !important;">Address 2</div>
                                <!-- Form Row        -->
                                <div class="row gx-3 mb-3">
                                    <!-- Form Group (organization name)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="inputOrgName">Province</label>
                                        <select name="province_2" class="form-control form-control-custom" id="province_2">
                                            <option value="0" style="text-align: center"> --- Select ---</option>
                                            @foreach($listProvince as $province)
                                                @if(count($address) == 2 && $address[1]['province_id'] == $province['code'])
                                                    <option value="{{$province['code']}}" selected>{{$province['name']}}</option>
                                                @else
                                                    <option value="{{$province['code']}}">{{$province['name']}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <!-- Form Group (location)-->
                                    <div class="col-md-6">
                                        <label class="small mb-1" for="inputOrgName">District</label>
                                        <select name="district_2" class="form-control form-control-custom" id="district_2">
                                            <option value="0" style="text-align: center"> --- Select ---</option>
                                            @if(count($address) == 2)
                                                @foreach($address[1]['district-relative'] as $district)
                                                    @if($district['code'] == $address[1]['district_id'])
                                                        <option value="{{$district['code']}}"
                                                                selected>{{$district['name']}}</option>
                                                    @else
                                                        <option value="{{$district['code']}}"
                                                                >{{$district['name']}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <!-- Form Group (email address)-->
                                <div class="mb-3">
                                    <label class="small mb-1" for="inputEmailAddress">Detail</label>
                                    <input class="form-control" id="inputEmailAddress" type="text" name="address_2"
                                           placeholder="Enter your detail" value="@if(count($address) > 1) {{$address[0]['address_line_1']}} @endif">
                                </div>
                                <!-- Save changes button-->
                                <button class="btn btn-primary" type="submit">Save changes</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <!-- Shop Sidebar End -->
    </div>
</div>
<!-- Topbar End -->


<!-- Footer Start -->
<div class="container-fluid bg-secondary text-dark mt-5 pt-5">
    @include('layouts.end-user.elements.footer')
</div>
<!-- Footer End -->


<!-- Back to Top -->
<a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('end-users/lib/easing/easing.min.js')}}"></script>
<script src="{{asset('end-users/lib/owlcarousel/owl.carousel.min.js')}}"></script>

<!-- Contact Javascript File -->
<script src="{{asset('end-users/mail/jqBootstrapValidation.min.js')}}"></script>
<script src="{{asset('end-users/mail/contact.js')}}"></script>

<!-- Template Javascript -->
<script src="{{asset('end-users/js/main.js')}}"></script>
<script>
    $('#province_1').change(function () {
        let provinceID = $(this).val();
        if(provinceID !== 0 ) {
            $.ajax({
                url: `{{route('get-district-follow-province')}}/${provinceID}`,
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    let textHtmlOption = `<option value="0" style="text-align: center"> --- Select ---</option>`;
                    $.each(data, function(index) {
                        textHtmlOption += `<option value="${data[index].code}" >${data[index].name} </option>`
                    });
                    $('#district_1').html(textHtmlOption);
                }
            });
        }
    });
    $('#province_2').change(function () {
        let provinceID = $(this).val();
        if(provinceID !== 0 ) {
            $.ajax({
                url: `{{route('get-district-follow-province')}}/${provinceID}`,
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    let textHtmlOption = `<option value="0" style="text-align: center"> --- Select ---</option>`;
                    $.each(data, function(index) {
                        textHtmlOption += `<option value="${data[index].code}" >${data[index].name} </option>`
                    });
                    $('#district_2').html(textHtmlOption);
                }
            });
        }
    });

    $('#upload-image').click(function () {
        $('#choose-image').trigger('click');
    });

    $('#choose-image').change(function (event) {
        $('.img-account-profile').attr( 'src',URL.createObjectURL(event.target.files[0]));
    });
</script>
</body>

</html>
