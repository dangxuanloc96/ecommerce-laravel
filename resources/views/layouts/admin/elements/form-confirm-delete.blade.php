<!-- Delete Confirm - Helper. Show a modal box -->
<div id="modalConfirmModelDelete" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="modalConfirmModelDeleteLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-red">
                <h4 class="modal-title"> Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p> Are you sure you want to delete this record?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnModalConfirmModelDelete" data-form-id="">Confirm
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCancelModelDelete">
                    Cancel
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

{{-- Initiate Confirm Delete --}}
<script type="text/javascript">
    $(function () {
        $(document).on('click', '.btnOpenerModalConfirmModelDelete', function (e) {
            e.preventDefault();
            var formId = $(this).attr('data-form-id');
            var btnConfirm = $('#modalConfirmModelDelete').find('#btnModalConfirmModelDelete');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', formId);
            }
            $('#modalConfirmModelDelete').modal('show');
        });
        // Modal Button Confirm Delete
        $(document).on('click', '#btnModalConfirmModelDelete', function (e) {
            e.preventDefault();
            var formId = $(this).attr('data-form-id');
            var form = $(document).find('#' + formId);
            if (form.length) {
                form.submit();
            }
            $('#modalConfirmModelDelete').modal('hide');
        });
        // Modal Button Cancel Delete
        $(document).on('click', '#modalConfirmModelDelete #btnModalCancelModelDelete', function (e) {
            e.preventDefault();
            var btnConfirm = $('#modalConfirmModelDelete').find('#btnModalConfirmModelDelete');
            if (btnConfirm.length) {
                btnConfirm.attr('data-form-id', "");
            }
            $('#modalConfirmModelDelete').modal('hide');
        });
    });
</script>
<!-- End Delete Confirm - Helper. Show a modal box -->
