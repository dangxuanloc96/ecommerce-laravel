<nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0">
    <a href="" class="text-decoration-none d-block d-lg-none">
        <h1 class="m-0 display-5 font-weight-semi-bold"><span
                class="text-primary font-weight-bold border px-3 mr-1">E</span>Shopper</h1>
    </a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
        <div class="navbar-nav mr-auto py-0">
            <a href="{{route('home')}}" class="nav-item nav-link active">Home</a>
            <a href="{{route('shop')}}" class="nav-item nav-link">Shop</a>
            <a href="{{route('detail')}}" class="nav-item nav-link">Shop Detail</a>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Pages</a>
                <div class="dropdown-menu rounded-0 m-0">
                    <a href="{{route('list-cart')}}" class="dropdown-item">Shopping Cart</a>
                    <a href="{{route('checkout')}}" class="dropdown-item">Checkout</a>
                </div>
            </div>
            <a href="{{route('contact')}}" class="nav-item nav-link">Contact</a>
        </div>
        @if(!\Illuminate\Support\Facades\Auth::check())
        <div class="navbar-nav ml-auto py-0">
            <a href="{{route('get-login')}}" class="nav-item nav-link">Login</a>
            <a href="{{route('get-register')}}" class="nav-item nav-link">Register</a>
        </div>
        @else
            <div class="navbar-nav ml-auto py-0" >
                <a href="{{route('edit-profile',\Illuminate\Support\Facades\Auth::user()->id)}}" class="nav-item nav-link"><i class="fa-solid fas fa-user"></i> {{\Illuminate\Support\Facades\Auth::user()->name}}</a>
            </div>
        @endif
    </div>
</nav>
