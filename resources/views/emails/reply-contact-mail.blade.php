<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Customer Feedback</h2>

<div>
    Thanks {{$details['name']}} for your feedback on our service.
</div>
<div>
    {{$details['reply']}}
</div>

</div>

</body>
</html>
