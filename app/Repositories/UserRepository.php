<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contract\IUserRepository;

class UserRepository extends BaseRepository implements IUserRepository
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }


    /**
     * @param $id
     * @return bool
     */
    public function activeUser($id) {
        $user = $this->model->find($id);
        $dataUpdate = ['status' => USER_STATUS_ACTIVE];

        if($user) {
            $this->model->where('id', $id)->update($dataUpdate);
            return true;
        }

        return false;
    }

}
