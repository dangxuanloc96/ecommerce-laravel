<?php

namespace App\Repositories;

use App\Models\ProductVariationOptionValue;
use App\Repositories\Contract\IProductVariationOptionValueRepository;

class ProductVariationOptionValueRepository extends BaseRepository implements IProductVariationOptionValueRepository
{
    protected $model;

    public function __construct(ProductVariationOptionValue $model)
    {
        $this->model = $model;
    }
}
