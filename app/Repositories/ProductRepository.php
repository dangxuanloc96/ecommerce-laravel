<?php

namespace App\Repositories;

use App\Models\Product;
use App\Repositories\Contract\IProductRepository;

class ProductRepository extends BaseRepository implements IProductRepository
{
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }
}
