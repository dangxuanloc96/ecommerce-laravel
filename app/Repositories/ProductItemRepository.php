<?php

namespace App\Repositories;

use App\Models\ProductItem;
use App\Repositories\Contract\IProductItemRepository;

class ProductItemRepository extends BaseRepository implements IProductItemRepository
{
    protected $model;

    public function __construct(ProductItem $model)
    {
        $this->model = $model;
    }
}
