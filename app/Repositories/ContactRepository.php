<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Repositories\Contract\IContactRepository;

class ContactRepository extends BaseRepository implements IContactRepository
{
    protected $model;

    public function __construct(Contact $model)
    {
        $this->model = $model;
    }
}
