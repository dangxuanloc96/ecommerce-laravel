<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Variation;
use App\Repositories\Contract\IUserRepository;
use App\Repositories\Contract\IVariationRepository;

class VariationRepository extends BaseRepository implements IVariationRepository
{
    protected $model;

    public function __construct(Variation $model)
    {
        $this->model = $model;
    }
}
