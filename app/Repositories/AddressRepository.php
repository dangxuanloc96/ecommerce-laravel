<?php

namespace App\Repositories;

use App\Models\Address;
use App\Repositories\Contract\IAddressRepository;

class AddressRepository extends BaseRepository implements IAddressRepository
{
    protected $model;

    public function __construct(Address $model)
    {
        $this->model = $model;
    }

}
