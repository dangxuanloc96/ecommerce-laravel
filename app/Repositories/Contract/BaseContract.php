<?php

namespace App\Repositories\Contract;

interface BaseContract
{
    /**
     * Find resource.
     *
     * @param mixed $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id);


    /**
     * @return mixed
     */
    public function findAll();

    /**
     * Create new resource.
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data);

    /**
     * Update existing resource.
     *
     * @param mixed $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data);

    /**
     * Delete existing resource.
     *
     * @param mixed $id
     * @return bool
     */
    public function delete($id);


    /**
     * @param array $data
     * @return mixed
     */
    public function insertMultiple(array $data);


    /**
     * @param $keySearch
     * @param $attributes
     * @return mixed
     */
    public function getAll($keySearch, $attributes, $conditions = []);

    public function findOneByConditions($conditions = []);
    public function deleteMultiple(array $conditions);
}
