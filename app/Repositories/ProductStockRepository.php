<?php

namespace App\Repositories;

use App\Models\ProductStock;
use App\Repositories\Contract\IProductStockRepository;

class ProductStockRepository extends BaseRepository implements IProductStockRepository
{
    protected $model;

    public function __construct(ProductStock $model)
    {
        $this->model = $model;
    }
}
