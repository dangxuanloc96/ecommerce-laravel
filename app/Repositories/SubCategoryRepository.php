<?php

namespace App\Repositories;

use App\Models\SubCategory;
use App\Repositories\Contract\ISubCategoryRepository;

class SubCategoryRepository extends BaseRepository implements ISubCategoryRepository
{
    protected $model;

    public function __construct(SubCategory $model)
    {
        $this->model = $model;
    }
}
