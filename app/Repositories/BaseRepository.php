<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Contract\BaseContract;

abstract class BaseRepository implements BaseContract
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Find resource.
     *
     * @param mixed $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function find($id)
    {
        return $this->model->find($id);
    }


    /**
     * @return mixed|void
     */
    public function findAll() {
        return $this->model->all();
    }

    /**
     * Create new resource.
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update existing resource.
     *
     * @param mixed $id
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data)
    {
        return $this->model->where('id', $id)->update($data);
    }

    /**
     * Delete existing resource.
     *
     * @param mixed $id
     * @return bool
     */
    public function delete($id)
    {
        return $this->model->where('id', $id)->delete()
                ? true : false;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function insertMultiple(array $data)
    {
        return $this->model->insert($data);
    }


    /**
     * @param $keySearch
     * @param $attributes
     * @return mixed
     */
    public function getAll($keySearch, $attributes, $conditions = [])
    {
        $query = $this->model->where(function ($query) use($keySearch, $attributes){
            foreach ($attributes as $attribute) {
                $query->orWhere($attribute, 'like', "%{$keySearch}%");
            }
        });

        $query->where(function ($query) use($conditions){
            foreach ($conditions as $key => $value) {
                $query->orWhere($key, $value);
            }
        });

        return $query->get();
    }

    public function findOneByConditions($conditions = []) {
        return $this->model->where(function ($query) use($conditions){
            foreach ($conditions as $key => $value) {
                $query->Where($key, $value);
            }
        })->first();
    }

    public function deleteMultiple(array $conditions) {
        return $this->model->where(function ($query) use($conditions){
            foreach ($conditions as $key => $value) {
                $query->where($key, $value);
            }
        })->get()->each(function ($product, $key) {
            $product->delete();
        });
    }
}
