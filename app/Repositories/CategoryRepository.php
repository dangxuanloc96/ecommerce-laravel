<?php

namespace App\Repositories;

use App\Models\Category;
use App\Repositories\Contract\ICategoryRepository;

class CategoryRepository extends BaseRepository implements ICategoryRepository
{
    protected $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }
}
