<?php

namespace App\Repositories;

use App\Models\ProductVariationOption;
use App\Repositories\Contract\IProductVariationOptionRepository;

class ProductVariationOptionRepository extends BaseRepository implements IProductVariationOptionRepository
{
    protected $model;

    public function __construct(ProductVariationOption $model)
    {
        $this->model = $model;
    }
}
