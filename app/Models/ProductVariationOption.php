<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVariationOption extends Model
{
    use HasFactory;
    protected $table = 'product_variation_options';
    protected $fillable = [
        'id',
        'product_id',
        'variation_id',
        'variation_name'
    ];

    public function productVariationOptionValues()
    {
        return $this->hasMany(ProductVariationOptionValue::class, 'product_variation_id', 'id');
    }
}
