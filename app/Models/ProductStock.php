<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    use HasFactory;
    protected $table = 'product_stocks';

    protected $fillable = [
        'id',
        'total_stock',
        'unit_price',
        'total_price',
        'product_item_id',
        'product_id',
        'available_stock'
    ];

    public function productItem()
    {
        return $this->belongsTo(ProductItem::class, 'product_item_id');
    }
}
