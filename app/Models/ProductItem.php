<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model
{
    use HasFactory;
    protected $table = 'product_items';

    protected $fillable = [
        'id',
        'image',
        'combination_string',
        'sku',
        'price',
        'product_id',
        'available_stock'
    ];

    public function productStocks()
    {
        return $this->hasMany(ProductStock::class, 'product_item_id', 'id');
    }
}
