<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVariationOptionValue extends Model
{
    use HasFactory;
    protected $table = 'product_variation_option_values';
    protected $fillable = [
        'id',
        'product_variation_id',
        'variation_option_id',
        'variation_name'
    ];

    public function productVariationOption()
    {
        return $this->belongsTo(ProductVariationOption::class, 'product_variation_id');
    }
}
