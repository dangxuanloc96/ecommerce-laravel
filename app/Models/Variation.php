<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variation extends Model
{
    use HasFactory;
    protected $table = 'variations';
    protected $fillable = ['variation_name'];

    public function variationOptions()
    {
        return $this->hasMany(VariationOption::class, 'variation_id', 'id');
    }
}
