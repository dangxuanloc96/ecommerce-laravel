<?php

namespace App\Http\Middleware;

use App\Facades\AuthAdmin;
use Closure;
use Illuminate\Http\Request;

class AdminCanable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (AuthAdmin::check()) {
            return $next($request);
        } else {
           return redirect()->route('admin.get-login');
        }
    }
}
