<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use App\Services\Contract\IProvinceService;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    private $provinceService;

    public function __construct(
        IProvinceService $provinceService
    )
    {
        $this->provinceService = $provinceService;
    }
    public function getListProvince() {
        return $this->provinceService->getListProvince();
    }

    public function getDistrictFollowProvince($provinceID){
        return $this->provinceService->getDistrictFollowProvince($provinceID);
    }
}
