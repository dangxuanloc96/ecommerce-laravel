<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use App\Services\Contract\IContactService;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contactService;
    public function __construct(
        IContactService $contactService
    )
    {
        $this->contactService = $contactService;
    }

    public function create(Request $request) {
        try {
            $dataInput = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'subject' => $request->input('subject'),
                'message' => $request->input('message'),
            ];

            $this->contactService->createContact($dataInput);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
