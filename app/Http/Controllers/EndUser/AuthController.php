<?php

namespace App\Http\Controllers\EndUser;

use App\Helpers\StorageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\EndUser\ProfileRequest;
use App\Http\Requests\EndUser\RegisterUserRequest;
use App\Services\Contract\IAddressService;
use App\Services\Contract\IProvinceService;
use App\Services\Contract\IUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    private $userService;
    private $provinceService;
    private $addressService;
    private static $pathStore = 'public/users';

    public function __construct(
        IUserService $userService, IProvinceService $provinceService, IAddressService $addressService
    )
    {
        $this->userService = $userService;
        $this->provinceService = $provinceService;
        $this->addressService = $addressService;
    }

    public function getRegister() {
        return view('end-users.auth.register-user');
    }

    public function postRegister(RegisterUserRequest $request) {
        try {
            $inputData = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => $request->input('password'),
                'role' => 0,
                'status' => USER_STATUS_UN_ACTIVE
            ];

            $this->userService->registerUser($inputData);
        } catch (\Exception $e) {
            return redirect()->route('get-register')->with('error', $e->getMessage());
        }
        return redirect()->route('get-register')->with('success', 'Register success, please verify your email to complete the registration');
    }

    public function getCompleteRegister($userId) {
        $this->userService->activeUser($userId);
        return redirect()->route('get-login')->with(['success' => "Register account successfully"]);
    }

    public function editProfile($userId) {
        $this->userService->checkExistUser($userId);
        $user = $this->userService->get($userId);
        $listProvince = $this->provinceService->getListProvince();
        $address = $this->addressService->getAddressUser($userId);
        $addressCustom = $this->provinceService->getListDistrictRelative($address->toArray());
        return view('end-users.user.profile-user')->with(['user' => $user, 'listProvince' => $listProvince, 'address' => $addressCustom]);
    }

    public function getLogin() {
        return view('end-users.auth.login-user');
    }

    public function postLogin(Request $request) {
        $login = [
            'email' =>   $request->email,
            'password' => $request->password,
        ];

        if(Auth::guard()->attempt($login, $request->remember)) {
            return redirect()->route('home');
        } else {
            return redirect()->back()->with('login-error', 'Email or Password is incorrect');
        }
    }

    public function updateProfile(ProfileRequest $request, $userId) {
        $dataInput = [
            'user_id' => $userId,
            'name' => $request->input('name'),
            'phone' => $request->input('phone'),
            'province_1' => $request->input('province_1'),
            'province_2' => $request->input('province_2'),
            'district_1' =>  $request->input('district_1'),
            'district_2' =>  $request->input('district_2'),
            'address_1' =>  $request->input('address_1'),
            'address_2' =>  $request->input('address_2'),
        ];


        try {
            $pathImage = StorageHelper::saveImage($request->file('image'), self::$pathStore);
            $dataInput ['image'] = $pathImage;

            DB::beginTransaction();
            $this->userService->updateProfile($dataInput, $userId);
            DB::commit();
        } catch (\Exception $e) {
            return redirect()->route('get-complete-register', $userId)->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('edit-profile', $userId);
    }

    public function redirectToFaceBook() {
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectToGoogle() {
        return Socialite::driver('google')->redirect();
    }

    public function callbackFromFaceBook()
    {
        try {
            $getInfo = Socialite::driver('facebook')->user();
            $user = $this->userService->createUserFromFaceBook($getInfo, 'facebook');
            auth()->login($user);
        } catch (\Exception $e) {
            dd($e);
        }
        return redirect()->route('home');

    }

    public function callbackFromGoogle()
    {
        try {
            $getInfo = Socialite::driver('google')->user();
            $user = $this->userService->createUserFromGoogle($getInfo, 'google');
            auth()->login($user);
        } catch (\Exception $e) {
            dd($e);
        }
        return redirect()->route('home');

    }
}
