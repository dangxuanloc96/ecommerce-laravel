<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function getLogin() {
        if(Auth::guard('admin')->check()) {
            return redirect()->route('admin::home');
        }
        return view('admin.auth.login');
    }

    public function postLogin(LoginRequest $request) {
        $login = [
            'email' =>   $request->email,
            'password' => $request->password,
        ];
        if(Auth::guard('admin')->attempt($login, $request->remember)) {
            return redirect()->route('admin::home');
        } else {
            return redirect()->back()->with('login-error', 'Email or Password is incorrect');
        }
    }

    public function logout(Request $request) {
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        return redirect()->route('admin.get-login');
    }
}
