<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateSubCategoryRequest;
use App\Services\Contract\ICategoryService;
use App\Services\Contract\ISubCategoryService;
use Illuminate\Http\Request;
use App\Http\Requests\CreateSubCategoryRequest;

class SubCategoryController extends Controller
{
    private $subCategoryService;
    private $categoryService;

    public function __construct(
        ISubCategoryService $subCategoryService, ICategoryService $categoryService
    )
    {
        $this->subCategoryService = $subCategoryService;
        $this->categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keySearch = $request->input('key_search');
        $listSubCategory = $this->subCategoryService->getAll($keySearch);
        return view('admin.sub-categories.index', ['listSubCategory' => $listSubCategory, 'keySearch' => $keySearch]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listCategory = $this->categoryService->getAll();
        return view('admin.sub-categories.create', ['listCategory' => $listCategory]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSubCategoryRequest $request)
    {
        try {
            $dataInput = ['name' => $request->input('name'), 'category_id' => $request->input('category_id')];
            $this->subCategoryService->createSubCategory($dataInput);
        } catch (\Exception $e) {
            return redirect()->route('admin::sub-category.create')->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::sub-category.index')->with('success', 'CREATE SUCCESS');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subCategory = $this->subCategoryService->getSubCategoryByID($id);
        $listCategory = $this->categoryService->getAll();
        return view('admin.sub-categories.edit', ['subCategory' => $subCategory, 'listCategory' => $listCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubCategoryRequest $request, $id)
    {
        try {
            $dataInput = ['name' => $request->input('name'), 'category_id' => $request->input('category_id')];

            $this->subCategoryService->updateSubCategory($id, $dataInput);
        } catch (\Exception $e) {
            return redirect()->route('admin::sub-category.edit', $id)->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::sub-category.index')->with('success', 'UPDATE SUCCESS');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $this->subCategoryService->deleteSubCategory($id);
        } catch (\Exception $ex) {
            return redirect()->route('admin::sub-category.index')->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::sub-category.index')->with('success', 'DELETE SUCCESS');
    }

    public function getListSubByCategoryID($categoryID) {
       return $this->subCategoryService->getListSubByCategoryID($categoryID);
    }

    public function getListSubByCategoryIDView($categoryID) {
        $listSubCategory = $this->subCategoryService->getListSubByCategoryIDView($categoryID);
        return view('admin.sub-categories.index', ['listSubCategory' => $listSubCategory, 'keySearch' => ""]);
    }
}
