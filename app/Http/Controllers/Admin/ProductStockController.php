<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Services\Contract\IProductItemService;
use App\Services\Contract\IProductStockService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductStockController extends Controller
{
    private $productStockService;
    private $productItemService;
    public function __construct(
        IProductStockService $productStockService,
        IProductItemService $productItemService
    )
    {
        $this->productStockService = $productStockService;
        $this->productItemService = $productItemService;
    }

    public function store(Request $request) {
        $dataInput = [
            'product_item_id' => $request->input('product_item_id'),
            'unit_price' => $request->input('unit_price'),
            'total_stock' => $request->input('quantity'),
            'total_price' => $request->input('unit_price') * $request->input('quantity')
        ];


        try {
            DB::beginTransaction();
            $this->productStockService->createProductStock($dataInput);
            $availableStock = $this->productItemService->addAvailableStock($request->input('product_item_id'), $request->input('quantity'));
            $totalStock = $this->productStockService->getTotalStockByProductItemID($request->input('product_item_id'));
            $totalPrice = $this->productStockService->getTotalPriceByProductItemID($request->input('product_item_id'));

        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
        DB::commit();
        return ['available_stock' => $availableStock, 'total_stock' => $totalStock, 'total_price' => $totalPrice];
    }

    public function listStockByproductItemID($productItemID) {
        try {
            return $this->productStockService->getListStockByProductItemID($productItemID)->toArray();
        } catch (\Exception $e) {
            dd($e);
            return false;
        }
    }
}
