<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateVariationRequest;
use App\Services\Contract\IVariationService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateVariationRequest;

class VariationController extends Controller
{
    private $variationService;

    public function __construct(
        IVariationService $variationService
    )
    {
        $this->variationService = $variationService;
    }
    public function index(Request $request) {
        $keySearch = $request->input('key_search');
        $listVariation = $this->variationService->getAll($keySearch);
        return view('admin.variations.index', ['listVariation' => $listVariation, 'keySearch' => $keySearch]);
    }

    public function create(){
        return view('admin.variations.create');
    }

    public function store(CreateVariationRequest $request){
        try {
            DB::beginTransaction();
            $data = [
                'name' =>   $request->input('name'),
                'options' =>   $request->input('options')
            ];
            $this->variationService->createVariation($data);
        } catch (Exception $ex) {
            DB::rollback();
            return view('admin.variations.create')->with('error', 'SYSTEM ERROR');
        }
        DB::commit();
        return redirect()->route('admin::variation.index')->with('success', 'CREATE SUCCESS');
    }

    public function edit($id) {
        $variation = $this->variationService->getDataEdit($id);
        return view('admin.variations.edit', ['variation' => $variation]);
    }

    public function delete($id) {
        try {
            DB::beginTransaction();
            $this->variationService->deleteVariation($id);
        } catch (Exception $ex) {
            DB::rollback();
            return view('admin.variations.edit')->with('error', 'SYSTEM ERROR');
        }
        DB::commit();
        return redirect()->route('admin::variation.index')->with('success', 'DELETE SUCCESS');
    }

    public function update($id, UpdateVariationRequest $request) {
        try {
            DB::beginTransaction();
            $data = ['name' => $request->input('name'), 'options' => $request->input('options')];
            $this->variationService->updateVariation($id, $data);
        } catch (Exception $ex) {
            DB::rollback();
            return view('admin.variations.edit')->with('error', 'SYSTEM ERROR');
        }
        DB::commit();
        return redirect()->route('admin::variation.index')->with('success', 'UPDATE SUCCESS');
    }

    public function listOption($variationID) {
       return $this->variationService->getListOptionByVariationID($variationID);
    }
}
