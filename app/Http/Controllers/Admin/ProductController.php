<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\StorageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Services\Contract\ICategoryService;
use App\Services\Contract\IProductService;
use App\Services\Contract\ISubCategoryService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productService;
    private $categoryService;
    private $subCategoryService;
    private static $pathStore = 'public/products';

    public function __construct(
        IProductService $productService, ICategoryService $categoryService, ISubCategoryService $subCategoryService
    )
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
        $this->subCategoryService = $subCategoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keySearch = $request->input('key_search');
        $listProduct = $this->productService->getAll($keySearch);
        return view('admin.products.index', ['listProduct' => $listProduct, 'keySearch' => $keySearch]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listCategory = $this->categoryService->getAll();
        return view('admin.products.create', ['listCategory' => $listCategory]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        try {
            $dataInput = [
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'category_id' => $request->input('category_id'),
                'sub_category_id' => $request->input('sub_category_id'),
            ];

            $pathImage = StorageHelper::saveImage($request->file('image'), self::$pathStore);
            $dataInput ['image'] = $pathImage;

            $this->productService->createProduct($dataInput);
        } catch (\Exception $e) {
            return redirect()->route('admin::product.create')->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::product.index')->with('success', 'CREATE SUCCESS');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->productService->findProductById($id);
        $listSubCategory = $this->subCategoryService->getListSubByCategoryID($product->category_id);
        $listCategory = $this->categoryService->getAll();
        return view('admin.products.edit', ['listSubCategory' => $listSubCategory, 'listCategory' => $listCategory, 'product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $dataInput = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'category_id' => $request->input('category_id'),
            'sub_category_id' => $request->input('sub_category_id'),
            'promotion_id' => $request->input('promotion_id'),
            'image' => StorageHelper::saveImage($request->file('image'), self::$pathStore)
        ];
        try {
            $this->productService->updateProduct($id, $dataInput);
        } catch (\Exception $e) {
            return redirect()->route('admin::product.edit', $id)->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::product.index')->with('success', 'CREATE SUCCESS');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $this->productService->deleteProduct($id);
        } catch (\Exception $e) {
            return redirect()->route('admin::product.index')->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::product.index')->with('success', 'DELETE SUCCESS');
    }
}
