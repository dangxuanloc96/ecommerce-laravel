<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Contract\IProductItemService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductItemController extends Controller
{
    private $productItemService;
    public function __construct(
        IProductItemService $productItemService
    )
    {
        $this->productItemService = $productItemService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($productID, Request $request)
    {
        $keySearch = $request->input('key_search');
        $listProductItems = $this->productItemService->getAll($keySearch, ['product_id' => $productID]);
        return view('admin.product-items.index', ['listProductItems' => $listProductItems, 'productID' => $productID, 'keySearch' => $keySearch]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $productID)
    {
        try {
            DB::beginTransaction();

            $dataInput = [
                'items_index' => $request->items_index,
                'product_id' => $productID,
                'images' => $request->images,
                'combinations' => $request->combinations
            ];

            $this->productItemService->createProductItem($dataInput);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('admin::product-variation.index', $productID)->with('error', "SYSTEM ERROR");
        }
        DB::commit();
        return redirect()->route('admin::product-items.index', $productID)->with('success', "CREATE SUCCESS");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $productId, $id)
    {
        try {
            $dataInput = ['price' => $request->input('price')];
            $this->productItemService->updateProductItem($productId, $id, $dataInput);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($productId, $id)
    {
        try {
            DB::beginTransaction();
            $this->productItemService->deleteProductItem($productId, $id);

        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('admin::product-items.index', $productId)->with('error', "SYSTEM ERROR");
        }
        DB::commit();
        return redirect()->route('admin::product-items.index', $productId)->with('success', "DELETE SUCCESS");
    }
}
