<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\StorageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Services\Contract\ICategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $categoryService;
    private static $pathCategoryStore = 'public/categories';

    public function __construct(
        ICategoryService $categoryService
    )
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $keySearch = $request->input('key_search');
        $listCategory = $this->categoryService->getAll($keySearch);
        return view('admin.categories.index', ['listCategory' => $listCategory, 'keySearch' => $keySearch]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        try {
            $dataInput = ['name' => $request->input('name')];
            $dataInput['category_icon'] = StorageHelper::saveImage($request->file('category_icon'), self::$pathCategoryStore);

            $this->categoryService->createCategory($dataInput);
        } catch (\Exception $e) {
            return redirect()->route('category.store')->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::category.index')->with('success', 'CREATE SUCCESS');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->categoryService->getCategoryByID($id);

        return view('admin.categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, $id)
    {
        try {
            $dataInput = ['name' => $request->input('name'), 'category_icon' => StorageHelper::saveImage($request->file('category_icon'), self::$pathCategoryStore)];

            $this->categoryService->updateCategory($id, $dataInput);
        } catch (\Exception $e) {
            return redirect()->route('admin::category.edit', $id)->with('error', 'SYSTEM ERROR');
        }

        return redirect()->route('admin::category.index')->with('success', 'UPDATE SUCCESS');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $this->categoryService->deleteCategory($id);
        } catch (\Exception $ex) {
            return redirect()->route('admin::category.index')->with('error', 'SYSTEM ERROR');
        }
        return redirect()->route('admin::category.index')->with('success', 'DELETE SUCCESS');
    }
}
