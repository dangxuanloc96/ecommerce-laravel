<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Contract\IProductVariationService;
use App\Services\Contract\IVariationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductVariationController extends Controller
{
    private $productVariationService;
    private $variationService;
    public function __construct(
        IProductVariationService $productVariationService, IVariationService $variationService
    )
    {
        $this->productVariationService = $productVariationService;
        $this->variationService = $variationService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $productId)
    {
        $keySearch = $request->input('key_search') ?? "";
        $listVariation = $this->variationService->getAll();
        $listProductVariation = $this->productVariationService->getListProductVariationByProductID($productId, $keySearch);
        $listProductVariationCombination = $this->productVariationService->generateProductVariationCombination($listProductVariation);

        return view('admin.product-variants.index', ['listProductVariation' => $listProductVariation, 'keySearch' => $keySearch, 'productID' => $productId, 'listVariation' => $listVariation, 'listProductVariationCombination' => $listProductVariationCombination]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($productID){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $productId)
    {
        try {
            DB::beginTransaction();

            $dataInput = [
                'variation_id' => $request->input('variation_id'),
                'variation_option_ids' => $request->input('variation_option_ids')
            ];

            $this->productVariationService->createProductVariationOption($productId, $dataInput);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->route('admin::product-variation.index', $productId)->with('error', "SYSTEM ERROR");
        }
        DB::commit();
        return redirect()->route('admin::product-variation.index', $productId)->with('success', "CREATE SUCCESS");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $productVariation = $this->productVariationService->get($id);
        $productID = $productVariation->product_id;
        try {
            DB::beginTransaction();
            $this->productVariationService->deleteProductVariation($id);
        } catch (\Exception $e) {
            dd($e->getMessage());
            DB::rollback();
            return  redirect()->route('admin::product-variation.index', $productID)->with('error', 'SYSTEM ERROR');
        }
        DB::commit();

        return  redirect()->route('admin::product-variation.index', $productID)->with('success', 'DELETE SUCCESS');
    }
}
