<?php

namespace App\Services;

use App\Models\Variation;
use App\Models\VariationOption;
use App\Repositories\Contract\IVariationRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IVariationService;
use ErrorException;

class VariationService extends AbstractService implements IVariationService
{
    private $variationOptionModel;
    public function __construct(IVariationRepository $variationRepository, Variation $variationModel, VariationOption $variationOptionModel)
    {
        $this->repository = $variationRepository;
        $this->model = $variationModel;
        $this->variationOptionModel = $variationOptionModel;
    }
    public function get($id)
    {
        return $this->repository->find($id);
    }

    public function getAll($keySearch = "")
    {
        $attributeSearch = ['variation_name'];
        return $this->repository->getAll($keySearch, $attributeSearch);
    }

    public function createVariation($data) {
        $variation = $this->model->where('variation_name', $data['name'])->get();

        if($variation->count() > 0) {
            throw new ErrorException('The record already exists');
        }

        $newVariation = $this->repository->create(['variation_name' => $data['name']]);

        $this->createVariationOptions($newVariation->id, $data['options']);
    }

    public function getDataEdit($id) {
        $variation = $this->repository->find($id);
        $data = [];
        if($variation) {
            $data['id'] = $variation->id;
            $data['name'] = $variation->variation_name;
            $options = [];

            foreach ($variation->variationOptions as $variationOption) {
                $options[] = $variationOption->variation_value;
            }

            $data['options'] = $options;
        }

        return $data;
    }

    public function updateVariation($id, $data) {
        $variation = $this->repository->find($id);

        if($variation) {
            $this->repository->update($id, ['variation_name' => $data['name']]);
            $this->variationOptionModel->where('variation_id', $id)->forceDelete();
            $this->createVariationOptions($id, $data['options']);
        }
    }

    private function createVariationOptions($variationId, $stringData) {
        $dataOptions = explode(';', $stringData);
        $dataUpdateOptions = [];

        foreach ($dataOptions as $option) {
            $dataUpdateOptions[] = [
                'variation_id' => $variationId,
                'variation_value' => $option
            ];
        }
        $this->variationOptionModel->insert($dataUpdateOptions);
    }

    public function deleteVariation($id) {
        $variation = $this->repository->find($id);
        if($variation) {
            $this->variationOptionModel->where('variation_id', $id)->delete();
            $this->repository->delete($id);
        }
    }

    public function getListOptionByVariationID($variationID){
        return $this->variationOptionModel->where('variation_id', $variationID)->get();
    }
}
