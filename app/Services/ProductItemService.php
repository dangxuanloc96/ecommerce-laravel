<?php

namespace App\Services;

use App\Helpers\StorageHelper;
use App\Models\Product;
use App\Models\ProductItem;
use App\Repositories\Contract\IProductItemRepository;
use App\Repositories\Contract\IProductStockRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IProductItemService;
use function League\Flysystem\toArray;


class ProductItemService extends AbstractService implements IProductItemService
{
    private static $pathProductItemStore = 'public/product-items';
    private $productStockRepository;
    public function __construct(IProductItemRepository $productItemRepository, ProductItem $productItemModel, IProductStockRepository $productStockRepository)
    {
        $this->repository = $productItemRepository;
        $this->model = $productItemModel;
        $this->productStockRepository = $productStockRepository;
    }

    public function getAll($keySearch = "", $conditions = []) {
        $attributeSearch = ['sku', 'price'];
        return $this->repository->getAll($keySearch, $attributeSearch, $conditions);
    }

    public function createProductItem($dataInput) {
        $listOldProductItems = $this->repository->getAll("", [], ['product_id' => $dataInput['product_id']])->pluck('sku')->toArray();
        $listProductItemCreate = [];
        foreach ($dataInput['items_index'] as $key => $value) {
            if($value == 'true' && !in_array( $dataInput['combinations'][$key], $listOldProductItems)) {
                $listProductItemCreate [] = [
                    'product_id' => $dataInput['product_id'],
                    'image' => StorageHelper::saveImage($dataInput['images'][$key], self::$pathProductItemStore),
                    'sku' => $dataInput['combinations'][$key]
                ];
            }
        }

        return $this->repository->insertMultiple($listProductItemCreate);
    }

    public function deleteProductItem($productId, $id) {
        $this->productStockRepository->deleteMultiple(['product_item_id' => $id]);
        $this->repository->deleteMultiple(['product_id' => $productId, 'id' => $id]);
    }

    public function updateProductItem($productId, $id, $dataInput) {
        $productItem = $this->repository->findOneByConditions(['product_id' => $productId, 'id' => $id]);

        if($productItem) {
            $this->repository->update($id, $dataInput);
        }
    }

    public function addAvailableStock($productItemId, $quantity) {
        $productItem = $this->repository->findOneByConditions(['id' => $productItemId]);

        if($productItem) {
            $dataProductItem = $productItem->toArray();
            $this->repository->update($productItemId, ['available_stock' => $dataProductItem['available_stock'] + $quantity]);
            return $dataProductItem['available_stock'] + $quantity;
        }

        return 0;
    }
}
