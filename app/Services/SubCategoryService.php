<?php

namespace App\Services;

use App\Models\Category;
use App\Models\SubCategory;
use App\Repositories\Contract\ISubCategoryRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\ISubCategoryService;

class SubCategoryService extends AbstractService implements ISubCategoryService
{
    public function __construct(ISubCategoryRepository $subCategoryRepository, SubCategory $subCategoryModel)
    {
        $this->repository = $subCategoryRepository;
        $this->model = $subCategoryModel;
    }

    public function getAll($keySearch = "") {
        $attributeSearch = ['name'];
        return $this->repository->getAll($keySearch, $attributeSearch);
    }

    public function createSubCategory($dataInput) {
        $dataSubCate = explode(';', $dataInput['name']);
        $dataSubCateCreate = [];

        foreach ($dataSubCate as $subName) {
            $dataSubCateCreate[] = [
                'name' => $subName,
                'category_id' => $dataInput['category_id']
            ];
        }
        return $this->repository->insertMultiple($dataSubCateCreate);
    }

    public function getSubCategoryByID($id) {
        return $this->repository->find($id);
    }

    public function updateSubCategory($id, $dataInput) {
        return $this->repository->update($id, $dataInput);
    }

    public function deleteSubCategory($id) {
        $category = $this->repository->find($id);
        if($category) {
            $this->repository->delete($id);
        }
    }

    public function getListSubByCategoryID($categoryID) {
        return $this->model->where('category_id', $categoryID)->get();
    }

    public function getListSubByCategoryIDView($categoryID) {
        return $this->repository->getAll('', [], ['category_id' => $categoryID]);
    }
}
