<?php

namespace App\Services;
use App\Models\Address;
use App\Repositories\Contract\IAddressRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IAddressService;
use App\Services\Contract\AbstractProxyService;
use GuzzleHttp\Client;

class AddressService extends AbstractService implements IAddressService
{
    public function __construct(IAddressRepository $addressRepository, Address $addressModel)
    {
        $this->repository = $addressRepository;
        $this->model = $addressModel;
    }

    public function createAddress($data) {
        $this->repository->insertMultiple($data);
    }

    public function getAddressUser($userID) {
        return  $this->model->where('user_id', $userID)->get();
    }

    public function updateAddress($id, $data) {
        return $this->repository->update($id, $data);
    }
}
