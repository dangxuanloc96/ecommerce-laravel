<?php

namespace App\Services;

use App\Models\Contact;
use App\Repositories\Contract\IContactRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IContactService;
use App\Services\Contract\IMailService;

class ContactService extends AbstractService implements IContactService
{
    private $mailService;
    public function __construct(IContactRepository $contactRepository, Contact $contactModel, IMailService $mailService)
    {
        $this->repository = $contactRepository;
        $this->model = $contactModel;
        $this->mailService = $mailService;
    }

    public function createContact($dataInput) {
        $this->repository->create($dataInput);
    }

    public function listContact($keySearch) {
        return $this->repository->getAll($keySearch, ['name', 'email']);
    }

    public function replyContact($contactID, $dataInput) {
        $contact = $this->repository->find($contactID);

        if($contact) {
            $contact->status = FEEDBACK;
            $contact->save();
            $this->mailService->replyContact($contact->name, $contact->email,  $dataInput['reply']);
        }
    }
}
