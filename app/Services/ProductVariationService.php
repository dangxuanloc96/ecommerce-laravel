<?php

namespace App\Services;

use App\Models\ProductVariationOption;
use App\Models\ProductVariationOptionValue;
use App\Repositories\Contract\IProductItemRepository;
use App\Repositories\Contract\IProductVariationOptionRepository;
use App\Repositories\Contract\IVariationRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IProductVariationService;
use App\Services\Contract\IVariationService;
use PHPUnit\Framework\Constraint\Count;
use function Symfony\Component\String\length;

class ProductVariationService extends AbstractService implements IProductVariationService
{
    private $variationService;
    private $productVariationOptionValueModel;
    private $productItemRepository;

    public function __construct(IProductVariationOptionRepository $productVariationOptionRepository, ProductVariationOption $productVariationOptionModel, ProductVariationOptionValue $productVariationOptionValueModel,
                                IVariationService $variationService, IProductItemRepository $productItemRepository)
    {
        $this->repository = $productVariationOptionRepository;
        $this->model = $productVariationOptionModel;
        $this->productVariationOptionValueModel = $productVariationOptionValueModel;
        $this->variationService = $variationService;
        $this->productItemRepository = $productItemRepository;
    }

    public function get($id) {
        return $this->repository->find($id);
    }

    public function getAll($keySearch = "") {
        $attributeSearch = ['name'];
        return $this->repository->getAll($keySearch, $attributeSearch);
    }

    public function getListProductVariationByProductID($productId, $keySearch = "") {
        return $this->repository->getAll('', [], ['product_id' => $productId]);
    }

    public function generateProductVariationCombination($listProductVariation) {
        $listProductVariationOptions = [];
        foreach ($listProductVariation as $productVariation) {
            $productVariationOption = [];
            foreach ($productVariation->productVariationOptionValues as $productVariationOptionValue) {
                $productVariationOption []  = $productVariationOptionValue->variation_name;
            }
            $listProductVariationOptions [] = $productVariationOption;
        }

        $str = "";
        $result = [];
        $this->generateProductItems($str, $listProductVariationOptions, $result);
        return $result;
    }

    private function generateProductItems($optionBefore, $listProductVariationOptions, &$result) {
        if(count($listProductVariationOptions) == 1) {
            foreach ($listProductVariationOptions[0] as $option) {
                $result [] = substr($optionBefore. "-" .$option, 1);
            }
        } else if(count($listProductVariationOptions) > 1) {
            $productVariationOption  = array_shift($listProductVariationOptions);
            foreach ($productVariationOption as $option) {
                $this->generateProductItems($optionBefore ."-".$option, $listProductVariationOptions, $result);
            }
        } else {
            return;
        }
    }

    public function createProductVariationOption($productId, $dataInput) {
        $variation = $this->variationService->get($dataInput['variation_id']);
        $listVariationOptions = $this->variationService->getListOptionByVariationID($dataInput['variation_id']);

        if($variation) {
            $dataInsert = ['product_id' => $productId, "variation_name" => $variation->variation_name, "variation_id" => $variation->id];
            $productVariation = $this->repository->findOneByConditions($dataInsert);

            if(!$productVariation) {
                $productVariation = $this->repository->create($dataInsert);
            }

            $dataProductVariationOptions = [];
            $productVariationOptions = $this->productVariationOptionValueModel->where('product_variation_id', $productVariation->id)->get();

            foreach ($dataInput['variation_option_ids'] as $option) {
                if(!$productVariationOptions->contains('variation_option_id', $option)) {
                    $variationOption = $listVariationOptions->filter(function ($value, $key) use($option) {
                        return $value->id == $option;
                    })->first();
                    $dataProductVariationOptions [] = [
                        'product_variation_id' => $productVariation->id,
                        'variation_option_id' => $option,
                        'variation_name' => $variationOption ? $variationOption->variation_value : ""
                    ];
                }
            }
            $this->productVariationOptionValueModel->insert($dataProductVariationOptions);
        }

    }

    public function deleteProductVariation($id) {
        $this->productVariationOptionValueModel->where('product_variation_id', $id)->delete();
        return $this->repository->find($id)->delete();
    }
}
