<?php

namespace App\Services;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IMailService;
use Illuminate\Support\Facades\Mail;

class MailService extends AbstractService implements IMailService
{
    public function registerUser($userInfo)
    {
        $details = [
            'name' => $userInfo->name,
            'link' => route('get-complete-register', $userInfo->id),
        ];

        Mail::to($userInfo['email'])->send(new \App\Mail\RegisterMail($details));
    }

    public function replyContact($name, $userEmail, $replyContent) {

        $details = [
            'name' => $name,
            'reply' => $replyContent,
        ];

        Mail::to($userEmail)->send(new \App\Mail\ReplyContactMail($details));
    }
}
