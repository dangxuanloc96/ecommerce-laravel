<?php

namespace App\Services;

use App\Models\Category;
use App\Repositories\Contract\ICategoryRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\ICategoryService;

class CategoryService extends AbstractService implements ICategoryService
{
    public function __construct(ICategoryRepository $categoryRepository, Category $categoryModel)
    {
        $this->repository = $categoryRepository;
        $this->model = $categoryModel;
    }

    public function getAll($keySearch = "") {
        $attributeSearch = ['name'];
        return $this->repository->getAll($keySearch, $attributeSearch);
    }

    public function createCategory($data) {
        return $this->repository->create($data);
    }

    public function getCategoryByID($id) {
        return $this->repository->find($id);
    }

    public function updateCategory($id, $dataInput) {
        $category = $this->repository->find($id);
        $dataInput['category_icon'] = $dataInput['category_icon'] ?? $category->category_icon;

        return $this->repository->update($id, $dataInput);
    }

    public function deleteCategory($id) {
        $category = $this->repository->find($id);
        if($category) {
            $this->repository->delete($id);
        }
    }
}
