<?php

namespace App\Services;

use App\Models\User;
use App\Services\Contract\IAddressService;
use App\Services\Contract\IUserService;
use App\Repositories\Contract\IUserRepository;
use App\Services\Contract\AbstractService;
use Illuminate\Hashing\BcryptHasher;
use App\Services\Contract\IMailService;

class UserService extends AbstractService implements IUserService
{
    private $mailService;
    private $addressService;
    /**
     * UserService constructor.
     * @param IUserRepository $userRepository
     * @param User $userModel
     */
    public function __construct(IUserRepository $userRepository, User $userModel, IMailService $mailService, IAddressService $addressService)
    {
        $this->repository = $userRepository;
        $this->model = $userModel;
        $this->mailService = $mailService;
        $this->addressService = $addressService;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }


    /**
     * @param $keySearch
     * @return mixed
     */
    public function getListUser($keySearch)
    {
        $attributeSearch = ['email', 'phone', 'name'];
        return $this->repository->getAll($keySearch, $attributeSearch);
    }

    /**
     * @param $id
     */
    public function delete($id) {
        $this->repository->delete($id);
    }

    /**
     * @param $id
     * @return bool
     */
    public function activeUser($id) {
        return $this->repository->activeUser($id);
    }


    /**
     * @param $data
     */
    public function registerUser($data) {
        $user = $this->model->where('email', $data['email'])->first();

        if($user && $user['status'] == USER_STATUS_ACTIVE) {
            throw new \Exception('This email existed');
        }

        if($user) {
            $this->model->where('email', $data['email'])->forceDelete();
        }

        $data['password'] = bcrypt($data['password']);
        $user = $this->repository->create($data);
        $this->mailService->registerUser($user);
    }

    public function updateProfile($data, $userID) {
        $user = $this->repository->find($userID);
        if(!$user || $user['status'] == USER_STATUS_UN_ACTIVE) {
            abort(404);
        }

        $this->repository->update($userID, [
            'name' => $data['name'],
            'phone' => $data['phone'],
            'image' => $data['image'] ? $data['image'] : $user->image
        ]);

        $oldListDataAddress = $this->addressService->getAddressUser($userID);

        if($data['province_1']) {
            $dataAddress= ['user_id' => $userID, 'province_id' => $data['province_1'], 'district_id' => $data['district_1'], 'address_line_1' => $data['address_1'], 'is_default' => true];
            if(count($oldListDataAddress) >= 1) {
                $this->addressService->updateAddress($oldListDataAddress[0]->id, $dataAddress);
            } else {
                $this->addressService->createAddress($dataAddress);
            }
        }

        if($data['province_2']) {
            $dataAddress = ['user_id' => $userID, 'province_id' => $data['province_2'], 'district_id' => $data['district_2'], 'address_line_1' => $data['address_2'], 'is_default' => false];
            if(count($oldListDataAddress) == 2) {
                $this->addressService->updateAddress($oldListDataAddress[1]->id, $dataAddress);
            } else {
                $this->addressService->createAddress($dataAddress);
            }
        }
    }

    public function checkExistUser($userID) {
        $user = $this->repository->find($userID);

        if(!$user || $user['status'] == USER_STATUS_UN_ACTIVE) {
            abort(404);
        }
        return true;
    }

    public function createUserFromFaceBook($getInfo,$provider) {
        $user = User::where('provider_id', $getInfo->id)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'role' => 0,
                'status' => USER_STATUS_ACTIVE,
                'provider' => $provider,
                'provider_id' => $getInfo->id,
                'password' => bcrypt("12345678")
            ]);
        }
        return $user;
    }

    public function createUserFromGoogle($getInfo,$provider) {
        $user = User::where('provider_id', $getInfo->id)->where('provider', $provider)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'role' => 0,
                'status' => USER_STATUS_ACTIVE,
                'provider' => $provider,
                'provider_id' => $getInfo->id,
                'password' => bcrypt("12345678")
            ]);
        }
        return $user;
    }
}
