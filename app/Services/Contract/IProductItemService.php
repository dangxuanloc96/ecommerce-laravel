<?php

namespace App\Services\Contract;

interface IProductItemService
{
    public function getAll($keySearch = "", $conditions = []);
    public function createProductItem($dataInput);
    public function deleteProductItem($productId, $id);
    public function updateProductItem($productId, $id, $dataInput);
    public function addAvailableStock($productItemId, $quantity);
}
