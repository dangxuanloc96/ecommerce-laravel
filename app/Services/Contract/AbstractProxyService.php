<?php

namespace App\Services\Contract;

use GuzzleHttp\Client;

abstract class AbstractProxyService
{
    protected $endpoint;
    protected $headers;
    protected $body;
    protected $queryString;
    protected $client;

    protected function create()
    {
        return $this->result($this->createRequest('post'));
    }

    protected function update()
    {
        return $this->result($this->createRequest('put'));
    }

    protected function get()
    {
        return $this->result($this->createRequest('get'));
    }

    protected function delete()
    {
        return $this->result($this->createRequest('delete'));
    }

    protected function createRequest($method) {
        $options = [];

        if(count($this->body) > 0) {
            $options['body'] = $this->body;
        }

        if(count($this->headers) > 0) {
            $options['headers'] = $this->headers;
        }

        if(count($this->queryString) > 0) {
            $options['query'] = $this->queryString;
        }

        return $this->client->request($method, $this->endpoint, $options);
    }

    private function result($request) {
        if($request->getStatusCode() == 200) {
            return json_decode($request->getBody(), true);
        }

        throw new \Exception('Your error message');
    }
}
