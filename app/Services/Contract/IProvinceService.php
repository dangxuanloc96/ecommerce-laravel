<?php

namespace App\Services\Contract;

interface IProvinceService
{
    public function getListProvince();
    public function getListDistrict();
    public function getDistrictFollowProvince($provinceID);
    public function getListDistrictRelative($address);
}
