<?php

namespace App\Services\Contract;

interface IContactService
{
    public function createContact($dataInput);
    public function listContact($keySearch);
    public function replyContact($contactID, $dataInput);
}
