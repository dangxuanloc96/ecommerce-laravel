<?php

namespace App\Services\Contract;

interface IUserService
{
    public function get($id);
    public function getListUser($keySearch);
    public function delete($id);
    public function activeUser($id);
    public function registerUser($data);
    public function updateProfile($data, $userID);
    public function checkExistUser($userID);
    public function createUserFromFaceBook($getInfo,$provider);
    public function createUserFromGoogle($getInfo,$provider);
}
