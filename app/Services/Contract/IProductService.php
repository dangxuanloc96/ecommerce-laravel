<?php

namespace App\Services\Contract;

interface IProductService
{
    public function getAll($keySearch = "");
    public function createProduct($dataInput);
    public function findProductById($id);
    public function updateProduct($id, $dataInput);
    public function deleteProduct($id);
}
