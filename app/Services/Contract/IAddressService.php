<?php

namespace App\Services\Contract;

interface IAddressService
{
    public function createAddress($data);
    public function getAddressUser($userID);
    public function updateAddress($id, $data);
}
