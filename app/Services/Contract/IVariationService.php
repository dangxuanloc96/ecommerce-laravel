<?php

namespace App\Services\Contract;

interface IVariationService
{
    public function get($id);
    public function getAll($keySearch = "");
    public function createVariation($data);
    public function getDataEdit($id);
    public function updateVariation($id, $data);
    public function deleteVariation($id);
    public function getListOptionByVariationID($variationID);
}
