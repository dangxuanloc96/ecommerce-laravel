<?php

namespace App\Services\Contract;

interface IProductVariationService
{
    public function get($id);
    public function getAll($keySearch = "");
    public function getListProductVariationByProductID($productId, $keySearch = "");
    public function generateProductVariationCombination($listProductVariation);
    public function createProductVariationOption($productId, $dataInput);
    public function deleteProductVariation($id);
}
