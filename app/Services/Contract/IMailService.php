<?php

namespace App\Services\Contract;

interface IMailService
{
    public function registerUser($userInfo);
    public function replyContact($name, $userEmail, $replyContent);
}
