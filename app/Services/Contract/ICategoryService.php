<?php

namespace App\Services\Contract;

interface ICategoryService
{
    public function getAll($keySearch = "");
    public function createCategory($data);
    public function getCategoryByID($id);
    public function updateCategory($id, $dataInput);
    public function deleteCategory($id);
}
