<?php

namespace App\Services\Contract;

interface IProductStockService
{
    public function getAll($keySearch = "");
    public function createProductStock($dataInput);
    public function getTotalStockByProductItemID($productItemID);
    public function getTotalPriceByProductItemID($productItemID);
    public function getListStockByProductItemID($productItemID);
}
