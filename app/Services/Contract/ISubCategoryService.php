<?php

namespace App\Services\Contract;

interface ISubCategoryService
{
    public function getAll($keySearch = "");
    public function createSubCategory($dataInput);
    public function getSubCategoryByID($id);
    public function updateSubCategory($id, $dataInput);
    public function deleteSubCategory($id);
    public function getListSubByCategoryID($categoryID);
    public function getListSubByCategoryIDView($categoryID);
}
