<?php

namespace App\Services;
use App\Services\Contract\IProvinceService;
use App\Services\Contract\AbstractProxyService;
use GuzzleHttp\Client;

class ProvinceService extends AbstractProxyService implements IProvinceService
{

    public function __construct()
    {
        $this->endpoint = '/api/';
        $this->headers = [];
        $this->body = [];
        $this->queryString = [];
        $this->client = new Client(['base_uri' => 'https://provinces.open-api.vn']);
    }

    public function getListProvince()
    {
        $this->queryString = ['depth' => 2];
        $data = $this->get();
        $dataResponse = [];

        foreach ($data as $province) {

            $districts = [];
            foreach ($province['districts'] as $district) {
                $districts [] = ['code' => $district['code'], 'name' => $district['name']];
            }

            $dataResponse[] = ['code' => $province['code'], 'name' => $province['name'], 'districts' => $districts];
        }

        return $dataResponse;
    }

    public function getListDistrict()
    {
        $this->queryString['query'] = ['depth' => 2];
        return $this->get();
    }

    public function getDistrictFollowProvince($provinceID)
    {
        $this->queryString = ['depth' => 2];
        $data = $this->get();
        $province = array_filter($data, function ($province) use($provinceID) {
            return $province['code'] == $provinceID;
        });
        if($province) {
            return array_values($province)[0]['districts'];
        }

        return [];
    }

    public function getListDistrictRelative($address) {
        foreach ($address as $key => $addr) {
            $address[$key]['district-relative'] = $this->getDistrictFollowProvince($addr['province_id']);
        }
        return $address;
    }
}
