<?php

namespace App\Services;

use App\Models\ProductStock;
use App\Repositories\Contract\IProductStockRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IProductStockService;


class ProductStockService extends AbstractService implements IProductStockService
{
    public function __construct(IProductStockRepository $productStockRepository, ProductStock $productStockModel)
    {
        $this->repository = $productStockRepository;
        $this->model = $productStockModel;
    }

    public function getAll($keySearch = "")
    {
    }

    public function createProductStock($dataInput) {
        return $this->repository->create($dataInput);
    }

    public function getTotalStockByProductItemID($productItemID) {
        return $this->repository->getAll('', [], ['product_item_id' => $productItemID])->sum('total_stock');
    }

    public function getTotalPriceByProductItemID($productItemID) {
        return $this->repository->getAll('', [], ['product_item_id' => $productItemID])->sum('total_price');
    }

    public function getListStockByProductItemID($productItemID) {
        return $this->repository->getAll('', [], ['product_item_id' => $productItemID]);
    }

}
