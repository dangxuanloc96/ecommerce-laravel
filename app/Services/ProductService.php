<?php

namespace App\Services;

use App\Models\Product;
use App\Repositories\Contract\IProductRepository;
use App\Services\Contract\AbstractService;
use App\Services\Contract\IProductService;


class ProductService extends AbstractService implements IProductService
{
    public function __construct(IProductRepository $productRepository, Product $productModel)
    {
        $this->repository = $productRepository;
        $this->model = $productModel;
    }

    public function getAll($keySearch = "") {
        $attributeSearch = ['name'];
        return $this->repository->getAll($keySearch, $attributeSearch);
    }

    public function createProduct($dataInput) {
        return $this->repository->create($dataInput);
    }

    public function findProductById($id) {
        return $this->repository->find($id);
    }

    public function updateProduct($id, $dataInput) {
        $product = $this->repository->find($id);
        $dataInput['image'] = $dataInput['image'] ?? $product->image;
        return $this->repository->update($id, $dataInput);
    }

    public function deleteProduct($id) {
        $product = $this->repository->find($id);
        if($product) {
            $product->delete($id);
        }
    }
}
