<?php

namespace App\Providers;

use App\Services\AddressService;
use App\Services\CategoryService;
use App\Services\ContactService;
use App\Services\Contract\ICategoryService;
use App\Services\Contract\IContactService;
use App\Services\Contract\IMailService;
use App\Services\Contract\IProductService;
use App\Services\Contract\IProductStockService;
use App\Services\Contract\IProductVariationService;
use App\Services\Contract\ISubCategoryService;
use App\Services\Contract\IVariationService;
use App\Services\ProductService;
use App\Services\ProductStockService;
use App\Services\ProductVariationService;
use App\Services\ProvinceService;
use App\Services\SubCategoryService;
use App\Services\UserService;
use App\Services\Contract\IUserService;
use Illuminate\Support\ServiceProvider;
use App\Services\VariationService;
use App\Services\MailService;
use App\Services\Contract\IProvinceService;
use App\Services\Contract\IAddressService;
use App\Services\Contract\IProductItemService;
use App\Services\ProductItemService;

class AdditionServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            IUserService::class,
            UserService::class
        );

        $this->app->bind(
            IVariationService::class,
            VariationService::class
        );

        $this->app->bind(
            IMailService::class,
            MailService::class
        );

        $this->app->bind(
            IProvinceService::class,
            ProvinceService::class
        );

        $this->app->bind(
            IAddressService::class,
            AddressService::class
        );

        $this->app->bind(
            ICategoryService::class,
            CategoryService::class
        );

        $this->app->bind(
            ISubCategoryService::class,
            SubCategoryService::class
        );

        $this->app->bind(
            IProductService::class,
            ProductService::class
        );

        $this->app->bind(
            IProductVariationService::class,
            ProductVariationService::class
        );

        $this->app->bind(
            IProductItemService::class,
            ProductItemService::class
        );

        $this->app->bind(
            IProductStockService::class,
            ProductStockService::class
        );

        $this->app->bind(
            IContactService::class,
            ContactService::class
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
