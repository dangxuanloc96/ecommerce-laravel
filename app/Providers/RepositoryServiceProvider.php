<?php

namespace App\Providers;

use App\Repositories\CategoryRepository;
use App\Repositories\ContactRepository;
use App\Repositories\Contract\ICategoryRepository;
use App\Repositories\Contract\IContactRepository;
use App\Repositories\Contract\IProductItemRepository;
use App\Repositories\Contract\IProductStockRepository;
use App\Repositories\ProductItemRepository;
use App\Repositories\Contract\IProductRepository;
use App\Repositories\Contract\IProductVariationOptionRepository;
use App\Repositories\Contract\IProductVariationOptionValueRepository;
use App\Repositories\Contract\ISubCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductStockRepository;
use App\Repositories\ProductVariationOptionRepository;
use App\Repositories\ProductVariationOptionValueRepository;
use App\Repositories\SubCategoryRepository;
use App\Repositories\UserRepository;
use App\Repositories\Contract\IUserRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Contract\IVariationRepository;
use App\Repositories\VariationRepository;
use App\Repositories\Contract\IAddressRepository;
use App\Repositories\AddressRepository;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            IUserRepository::class,
            UserRepository::class
        );

        $this->app->bind(
            IVariationRepository::class,
            VariationRepository::class
        );

        $this->app->bind(
            IAddressRepository::class,
            AddressRepository::class
        );

        $this->app->bind(
            ICategoryRepository::class,
            CategoryRepository::class
        );

        $this->app->bind(
            ISubCategoryRepository::class,
            SubCategoryRepository::class
        );

        $this->app->bind(
            IProductRepository::class,
            ProductRepository::class
        );

        $this->app->bind(
            IProductVariationOptionRepository::class,
            ProductVariationOptionRepository::class
        );

        $this->app->bind(
            IProductVariationOptionValueRepository::class,
            ProductVariationOptionValueRepository::class
        );

        $this->app->bind(
            IProductItemRepository::class,
            ProductItemRepository::class
        );

        $this->app->bind(
            IProductStockRepository::class,
            ProductStockRepository::class
        );

        $this->app->bind(
            IContactRepository::class,
            ContactRepository::class
        );
    }
}
