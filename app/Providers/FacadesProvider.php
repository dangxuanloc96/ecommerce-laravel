<?php

namespace App\Providers;

use App\Helper\ImageHelper;
use App\Helper\MetaTagHelper;
use App\Helpers\AuthAdminHelper;
use App\Helpers\AuthApiHelper;
use App\Helpers\DatabaseHelper;
use App\Helpers\DateTimeHelper;
use App\Helpers\SendMailHelper;
use App\Helpers\StringHelper;
use App\Helpers\UrlHelper;
use Illuminate\Support\ServiceProvider;

class FacadesProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('auth_admin_helper', function () {
            return new AuthAdminHelper();
        });

        $this->app->singleton('manage_file_helper', function () {
            return new AuthAdminHelper();
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
