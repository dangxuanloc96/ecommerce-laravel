<?php

namespace App\Providers;
use App\Models\Category;
use App\Models\ProductVariationOption;
use App\Observers\CategoryObserver;
use App\Observers\ProductVariationObserver;
use Carbon\Laravel\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Category::observe(CategoryObserver::class);
        ProductVariationOption::observe(ProductVariationObserver::class);
    }
}
