<?php

namespace App\Observers;

use App\Models\ProductVariationOption;

class ProductVariationObserver
{
    public $afterCommit = true;
    /**
     * Handle the ProductVariationOption "created" event.
     *
     * @param  \App\Models\ProductVariationOption  $productVariationOption
     * @return void
     */
    public function created(ProductVariationOption $productVariationOption)
    {
        //
    }

    public function creating(ProductVariationOption $productVariationOption)
    {
    }

    /**
     * Handle the ProductVariationOption "updated" event.
     *
     * @param  \App\Models\ProductVariationOption  $productVariationOption
     * @return void
     */
    public function updated(ProductVariationOption $productVariationOption)
    {
        //
    }

    /**
     * Handle the ProductVariationOption "deleted" event.
     *
     * @param  \App\Models\ProductVariationOption  $productVariationOption
     * @return void
     */
    public function deleting(ProductVariationOption $productVariationOption)
    {
        $productVariationOption->productVariationOptionValues()->forceDelete();
    }

    /**
     * Handle the ProductVariationOption "restored" event.
     *
     * @param  \App\Models\ProductVariationOption  $productVariationOption
     * @return void
     */
    public function restored(ProductVariationOption $productVariationOption)
    {
        //
    }

    /**
     * Handle the ProductVariationOption "force deleted" event.
     *
     * @param  \App\Models\ProductVariationOption  $productVariationOption
     * @return void
     */
    public function forceDeleted(ProductVariationOption $productVariationOption)
    {
        //
    }
}
