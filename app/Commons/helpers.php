<?php
 function convertArrayToString($array, $separatorCharacter) {

 }

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}

function displayMoney($money) {
     $characters = str_split(strrev($money));
     $result = "";
     foreach ($characters as $i => $character) {
         if($i != 0 && $i %3 == 0) {
             $result .= ".".$character;
         } else {
             $result .= $character;
         }
     }

     return strrev($result);
}
