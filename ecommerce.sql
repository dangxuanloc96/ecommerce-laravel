/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80030
 Source Host           : localhost:3306
 Source Schema         : ecommerce

 Target Server Type    : MySQL
 Target Server Version : 80030
 File Encoding         : 65001

 Date: 20/06/2023 22:26:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(0) UNSIGNED NOT NULL,
  `province_id` bigint(0) UNSIGNED NOT NULL,
  `district_id` bigint(0) UNSIGNED NOT NULL,
  `address_line_1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_line_2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `address_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `address_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` tinyint(0) NOT NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `admins_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admins
-- ----------------------------
INSERT INTO `admins` VALUES (1, 0, 'SuperAdmin', 'admin@admin.com', NULL, '$2y$10$6XiXJ.0nQtAPo2/w8MxypOkBV0516lgtEyvgX4xhcyZ46SVqCLqpi', NULL, '2023-06-17 08:12:38', '2023-06-17 08:12:38', NULL);

-- ----------------------------
-- Table structure for cart_items
-- ----------------------------
DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE `cart_items`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cart_id` bigint(0) UNSIGNED NOT NULL,
  `product_item_id` bigint(0) UNSIGNED NOT NULL,
  `quantity` int(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cart_items_cart_id_foreign`(`cart_id`) USING BTREE,
  INDEX `cart_items_product_item_id_foreign`(`product_item_id`) USING BTREE,
  CONSTRAINT `cart_items_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `cart_items_product_item_id_foreign` FOREIGN KEY (`product_item_id`) REFERENCES `product_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for carts
-- ----------------------------
DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(0) UNSIGNED NOT NULL,
  `quantity` int(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `carts_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (10, 'Áo nam', 'categories/RTEme0g6J6YPHyvtcpG9WKquM4TN3RCjmx2HDXjR.png', '2023-06-17 13:45:58', '2023-06-17 13:45:58', NULL);
INSERT INTO `categories` VALUES (11, 'Quần nam', 'categories/HbXYP0zG5jqhkGu5hHUVEtHFF7YYnQgJhfpg7Elp.png', '2023-06-17 13:46:21', '2023-06-17 13:46:21', NULL);
INSERT INTO `categories` VALUES (12, 'váy', 'categories/CUFkO0GO1x5Qq8AZtLLPuCcls4isdXdjlhuDW0tX.jpg', '2023-06-18 07:34:31', '2023-06-18 07:56:29', '2023-06-18 07:56:29');

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 'Đặng Xuân Lộc', 'dangxuanloc96@gmail.com', 'Dịch vụ khách hàng', 'Oke', 1, '2023-06-17 10:35:55', '2023-06-18 14:05:13');
INSERT INTO `contacts` VALUES (2, 'Đặng Xuân Lộc', 'dangxuanloc96@gmail.com', 'Chất lượng dịch vụ', 'Bán hàng như cức', 1, '2023-06-18 07:50:36', '2023-06-20 13:34:02');

-- ----------------------------
-- Table structure for districts
-- ----------------------------
DROP TABLE IF EXISTS `districts`;
CREATE TABLE `districts`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `province_id` bigint(0) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `districts_province_id_foreign`(`province_id`) USING BTREE,
  CONSTRAINT `districts_province_id_foreign` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(0) UNSIGNED NOT NULL,
  `reserved_at` int(0) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(0) UNSIGNED NOT NULL,
  `created_at` int(0) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jobs_queue_index`(`queue`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2020_11_04_135503_create_jobs_table', 1);
INSERT INTO `migrations` VALUES (6, '2021_01_13_044130_create_admins_table', 1);
INSERT INTO `migrations` VALUES (7, '2023_04_29_145400_create_provinces_table', 1);
INSERT INTO `migrations` VALUES (8, '2023_04_29_145401_create_districts_table', 1);
INSERT INTO `migrations` VALUES (9, '2023_04_29_145501_create_address_table', 1);
INSERT INTO `migrations` VALUES (10, '2023_04_29_145601_create_carts_table', 1);
INSERT INTO `migrations` VALUES (11, '2023_04_29_145732_create_promotions_table', 1);
INSERT INTO `migrations` VALUES (12, '2023_04_29_145755_create_categories_table', 1);
INSERT INTO `migrations` VALUES (13, '2023_04_29_145846_create_sub_categories_table', 1);
INSERT INTO `migrations` VALUES (14, '2023_04_29_150235_create_products_table', 1);
INSERT INTO `migrations` VALUES (15, '2023_04_29_150324_create_product_variation_options_table', 1);
INSERT INTO `migrations` VALUES (16, '2023_04_29_150406_create_product_variation_option_values_table', 1);
INSERT INTO `migrations` VALUES (17, '2023_04_29_150452_create_product_items_table', 1);
INSERT INTO `migrations` VALUES (18, '2023_04_29_150518_create_product_stocks_table', 1);
INSERT INTO `migrations` VALUES (19, '2023_04_29_150554_create_orders_table', 1);
INSERT INTO `migrations` VALUES (20, '2023_04_29_150654_create_oder_details_table', 1);
INSERT INTO `migrations` VALUES (21, '2023_04_29_150734_create_cart_items_table', 1);
INSERT INTO `migrations` VALUES (22, '2023_04_29_150831_create_user_reviews_table', 1);
INSERT INTO `migrations` VALUES (23, '2023_04_29_150900_create_variations_table', 1);
INSERT INTO `migrations` VALUES (24, '2023_04_29_150924_create_variation_options_table', 1);
INSERT INTO `migrations` VALUES (25, '2023_06_17_095713_create_contacts_table', 2);
INSERT INTO `migrations` VALUES (26, '2023_06_17_163727_add_provider_and_provider_id_to_users', 3);

-- ----------------------------
-- Table structure for order_details
-- ----------------------------
DROP TABLE IF EXISTS `order_details`;
CREATE TABLE `order_details`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_item_id` bigint(0) UNSIGNED NOT NULL,
  `order_id` bigint(0) UNSIGNED NOT NULL,
  `quantity` int(0) NOT NULL,
  `price` int(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `order_details_product_item_id_foreign`(`product_item_id`) USING BTREE,
  INDEX `order_details_order_id_foreign`(`order_id`) USING BTREE,
  CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_details_product_item_id_foreign` FOREIGN KEY (`product_item_id`) REFERENCES `product_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL,
  `payment_method` int(0) NOT NULL,
  `order_status` int(0) NOT NULL,
  `shipping_address` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_method` int(0) NOT NULL,
  `order_total` int(0) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(0) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `expires_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for product_items
-- ----------------------------
DROP TABLE IF EXISTS `product_items`;
CREATE TABLE `product_items`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sku` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(0) NULL DEFAULT NULL,
  `product_id` bigint(0) UNSIGNED NOT NULL,
  `available_stock` int(0) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_items_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `product_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_items
-- ----------------------------
INSERT INTO `product_items` VALUES (6, 'product-items/6MhKt62IyaUpspwJr07hjGFSKtvEQb7mtMZvtFkn.png', '2-L-Yellow', 250000, 2, 100, NULL, '2023-06-20 13:41:35', NULL);
INSERT INTO `product_items` VALUES (7, 'product-items/6Ct4q20v0c75GjqMNmmnreYlalDW1ycR6n5kKrml.png', '3-S-Blue', 500000, 3, 100, NULL, '2023-06-17 14:31:20', NULL);
INSERT INTO `product_items` VALUES (8, 'product-items/YEPAjkinSevurbJYK9nkRk7Aym5ewSOqKEqVSFH9.png', '3-M-Blue', 500000, 3, 100, NULL, '2023-06-17 14:31:27', NULL);
INSERT INTO `product_items` VALUES (9, 'product-items/bg2QY0LlmHQGuTIzL4lahwjrL1d4qBGCzELNVe7H.png', '3-L-Blue', 500000, 3, 100, NULL, '2023-06-17 14:31:38', NULL);
INSERT INTO `product_items` VALUES (10, 'product-items/J4eumWz74RwU3PyKAZ9W1IQnprw46KxoMU23Wpi4.jpg', '4-S-Blue', 200000, 4, 200, NULL, '2023-06-18 07:42:35', NULL);
INSERT INTO `product_items` VALUES (11, 'product-items/LXSWqhuHUpaSwGbKGhiv4HPrCqBLdmfCYmderm22.jpg', '4-S-Red', 200000, 4, 0, NULL, '2023-06-18 07:41:10', NULL);
INSERT INTO `product_items` VALUES (12, 'product-items/MiduQWB10cL9n6R1zRf2MPr17WNVMvngMwPNAKnE.jpg', '4-M-Blue', 200000, 4, 0, NULL, '2023-06-18 07:41:11', NULL);
INSERT INTO `product_items` VALUES (13, 'product-items/tBXgJ8OCbl18Vjjo2wyzQw7YV9LyTeVZFPijIcjc.jpg', '4-M-Red', 200000, 4, 0, NULL, '2023-06-18 07:41:13', NULL);
INSERT INTO `product_items` VALUES (14, 'product-items/CHNQJD7ZSq5rnTywOgUqh0h3zT105WeoEVqHbHmS.jpg', '4-L-Blue', 200000, 4, 0, NULL, '2023-06-18 07:41:14', NULL);
INSERT INTO `product_items` VALUES (15, 'product-items/OwHZHII9tiERNYqLnpNo324mabf1mrliLXDseoYK.jpg', '4-L-Red', 200000, 4, 0, NULL, '2023-06-18 07:41:14', NULL);
INSERT INTO `product_items` VALUES (16, 'product-items/Sae8IWkdcNfzL3udQxdJjxlTZAljc9OeN2MGYogt.png', '2-S-Yellow', 250000, 2, 100, NULL, '2023-06-20 13:41:50', NULL);
INSERT INTO `product_items` VALUES (17, 'product-items/Imv5K3It9sl70tgVKExnyqpXatsGqVHXWRRDCmvQ.png', '2-M-Yellow', 250000, 2, 100, NULL, '2023-06-20 13:42:01', NULL);
INSERT INTO `product_items` VALUES (24, 'product-items/f2NdPhC0ygeFO7YV8E79G67GRCLqdQrICUKR1iCg.png', '5-S-White', 480000, 5, 200, NULL, '2023-06-20 13:53:20', NULL);
INSERT INTO `product_items` VALUES (25, 'product-items/eMUwAdtAejiXBGBrjqdto3oQs937rqqXWuBgV0Vd.png', '5-S-Black', 480000, 5, 200, NULL, '2023-06-20 14:05:17', NULL);
INSERT INTO `product_items` VALUES (26, 'product-items/nVNMDRaoEXmq2QwQL0KHZOHEszbjRB4mSXCXt5Rw.png', '5-M-White', 480000, 5, 200, NULL, '2023-06-20 14:05:16', NULL);
INSERT INTO `product_items` VALUES (27, 'product-items/PZDEVJlPU1J59PVvTCipGWZQ8q0UNiOXDSetztyQ.png', '5-M-Black', 480000, 5, 200, NULL, '2023-06-20 13:53:40', NULL);
INSERT INTO `product_items` VALUES (28, 'product-items/ZY1WZU5VdjXoqWtgP1q04agVMCz4uwnzOMIoCmrj.png', '5-L-White', 480000, 5, 200, NULL, '2023-06-20 13:53:46', NULL);
INSERT INTO `product_items` VALUES (29, 'product-items/OIcYVftkKzhpDj2Jqs3aAOCB2h82uhGgsQjhPPH9.png', '5-L-Black', 480000, 5, 200, NULL, '2023-06-20 13:53:52', NULL);
INSERT INTO `product_items` VALUES (30, 'product-items/lbjeyV7HDDWNjFBR6lMFhFK4AV7fwxrOgOYqDoNJ.png', '6-S-Blue', 520000, 6, 300, NULL, '2023-06-20 14:10:14', NULL);
INSERT INTO `product_items` VALUES (31, 'product-items/QEdSsNzlkvy9Ruk3KLseuQBnwBlcwggUI7VhNBoN.png', '6-S-Orange', 520000, 6, 300, NULL, '2023-06-20 14:10:20', NULL);
INSERT INTO `product_items` VALUES (32, 'product-items/dRbxMObxujFa87FXkNow0HMKls1sjGDuCtgA09c0.png', '6-M-Blue', 520000, 6, 300, NULL, '2023-06-20 14:10:25', NULL);
INSERT INTO `product_items` VALUES (33, 'product-items/lllXhI4eEKct2M5A5h2dhndByEC9HWyn18xCx5Ep.png', '6-M-Orange', 520000, 6, 300, NULL, '2023-06-20 14:10:31', NULL);
INSERT INTO `product_items` VALUES (34, 'product-items/oAQSDidcLdKrLI2ZEvlG6IJpecM88rZJSirKkHnx.png', '6-L-Blue', 520000, 6, 300, NULL, '2023-06-20 14:10:36', NULL);
INSERT INTO `product_items` VALUES (35, 'product-items/gopbx6oTCPtihPxfaBy8qvgMFS6V3es2qA5OKF3P.png', '6-L-Orange', 520000, 6, 300, NULL, '2023-06-20 14:10:43', NULL);
INSERT INTO `product_items` VALUES (36, 'product-items/98OKuLOw8NcxSojtJFGW7xXrq8WuMhkp7sUas7k7.png', '7-S-White', 380000, 7, 100, NULL, '2023-06-20 14:17:04', NULL);
INSERT INTO `product_items` VALUES (37, 'product-items/jojUvtm9QrO1qhfM4VQejN37uAnyjPX8g4iCFF70.png', '7-S-Black', 380000, 7, 100, NULL, '2023-06-20 14:17:09', NULL);
INSERT INTO `product_items` VALUES (38, 'product-items/6FvTdtnEArdQYbo4ePzLy1gaCREakstcYdAJdwp4.png', '7-M-White', 380000, 7, 100, NULL, '2023-06-20 14:17:15', NULL);
INSERT INTO `product_items` VALUES (39, 'product-items/EfBD3QKP82cQ03vKIq0eYwWxedaokVe6H4Yiq8fK.png', '7-M-Black', 380000, 7, 100, NULL, '2023-06-20 14:17:20', NULL);
INSERT INTO `product_items` VALUES (40, 'product-items/zpcxAb09HpEakIM9ZOkWZwgJYhZIvajAXX9iCSPM.png', '7-L-White', 380000, 7, 100, NULL, '2023-06-20 14:17:26', NULL);
INSERT INTO `product_items` VALUES (41, 'product-items/BwlDGQWYzYyjEbfClKdmLQhB786bmgbu27BhjRNw.png', '7-L-Black', 380000, 7, 100, NULL, '2023-06-20 14:17:32', NULL);
INSERT INTO `product_items` VALUES (42, 'product-items/XyiBuk0JqxvM1wD5L19EfKxtxI30aNy5HV87jXgt.png', '8-S-White', 200000, 8, 100, NULL, '2023-06-20 14:23:10', NULL);
INSERT INTO `product_items` VALUES (43, 'product-items/NLwOARF6pBAvx4bOXeDyQWni5u3paiMv9H6PZkDx.png', '8-S-Black', 200000, 8, 100, NULL, '2023-06-20 14:23:11', NULL);
INSERT INTO `product_items` VALUES (44, 'product-items/K8yzBc2OigR08TtHxpEjS3Iy2rkVKY18CLWOiO6c.png', '8-M-White', 200000, 8, 100, NULL, '2023-06-20 14:23:13', NULL);
INSERT INTO `product_items` VALUES (45, 'product-items/XIsUOWH5FEaRamKM7uERIoIGaoBQzopjDTBNXhFN.png', '8-M-Black', 200000, 8, 100, NULL, '2023-06-20 14:23:14', NULL);
INSERT INTO `product_items` VALUES (46, 'product-items/Ok2Tz1TW2djqBt84ycaTexb4BcRth37Ki80laTEM.png', '8-L-White', 200000, 8, 100, NULL, '2023-06-20 14:23:15', NULL);
INSERT INTO `product_items` VALUES (47, 'product-items/wrpFXCevUSydVJKJrMkvsQ3kt8WuKD6M52nDQxBz.png', '8-L-Black', 200000, 8, 100, NULL, '2023-06-20 14:23:15', NULL);
INSERT INTO `product_items` VALUES (48, 'product-items/02E0QppmlzqSlj77Yq52Wyfik5B2uEcuNfVi3B33.png', '9-S-Blue', 620000, 9, 150, NULL, '2023-06-20 14:29:48', NULL);
INSERT INTO `product_items` VALUES (49, 'product-items/8vLxWy7kngYQhVJuo9LwdIKJcbuE6kdWFPY1ItEg.png', '9-S-White', 620000, 9, 150, NULL, '2023-06-20 14:29:53', NULL);
INSERT INTO `product_items` VALUES (50, 'product-items/KRxtjjElpGWm9I4ZeKMWEkQV6eVn2rAvlgCQXFEk.png', '9-M-Blue', 620000, 9, 150, NULL, '2023-06-20 14:30:00', NULL);
INSERT INTO `product_items` VALUES (51, 'product-items/n2M3t3q1EQqmQZNUrsrdbuEtPTcQ7evZupzApaZ9.png', '9-M-White', 620000, 9, 150, NULL, '2023-06-20 14:30:06', NULL);
INSERT INTO `product_items` VALUES (52, 'product-items/Rldy8aQKUmAJRgJij3V1kSpcc1PEVzHNG2knD9zn.png', '9-L-Blue', 620000, 9, 150, NULL, '2023-06-20 14:30:15', NULL);
INSERT INTO `product_items` VALUES (53, 'product-items/x6wFVu3boOsUdASIK331WRJnQr7LbmrlC2NuhnTO.png', '9-L-White', 620000, 9, 150, NULL, '2023-06-20 14:30:25', NULL);
INSERT INTO `product_items` VALUES (54, 'product-items/rWQ7wKYbNgWHLZGxXDzwn9dSv046f3h2szP9eAWh.png', '10-S-White', 450000, 10, 200, NULL, '2023-06-20 14:34:01', NULL);
INSERT INTO `product_items` VALUES (55, 'product-items/Jzp7zS3VECerKTwQ7CJGLFwgmbhKSNNdvbeNrSts.png', '10-S-Black', 450000, 10, 200, NULL, '2023-06-20 14:34:21', NULL);
INSERT INTO `product_items` VALUES (56, 'product-items/bYLqysnoqjPYMrXvTnMgXAyc0XjZ79v4y2JDUsG9.png', '10-M-White', 450000, 10, 200, NULL, '2023-06-20 14:34:26', NULL);
INSERT INTO `product_items` VALUES (57, 'product-items/Yq9bl8ZkSsGW5jjVURJfgGwjrCo4KgzjitszaE7I.png', '10-M-Black', 450000, 10, 200, NULL, '2023-06-20 14:34:32', NULL);
INSERT INTO `product_items` VALUES (58, 'product-items/3HVpHuK0TCzTDZDPmTtt4USKXXKMA1MxyTWvVezF.png', '10-L-White', 450000, 10, 200, NULL, '2023-06-20 14:34:38', NULL);
INSERT INTO `product_items` VALUES (59, 'product-items/9ISquMEoBqskG96fv1rPTszhqeyfg6SNTLY7WNzy.png', '10-L-Black', 450000, 10, 200, NULL, '2023-06-20 14:34:44', NULL);
INSERT INTO `product_items` VALUES (60, 'product-items/gZhe6QFrmnuDwrFrsxGqs3Y6ikU4RAl4txuCmjXL.png', '11-S-White', 470000, 11, 100, NULL, '2023-06-20 14:47:02', NULL);
INSERT INTO `product_items` VALUES (61, 'product-items/5CjPY8VvWhoTda2Vn7tQKgI3d8rpTKJBgWZxTadq.png', '11-S-  Purple', 470000, 11, 100, NULL, '2023-06-20 14:47:25', NULL);
INSERT INTO `product_items` VALUES (62, 'product-items/sPNKlje1BXFEBH3DYp9BXMxfTkgpKfA3zkod9cGL.png', '11-M-White', 470000, 11, 100, NULL, '2023-06-20 14:47:30', NULL);
INSERT INTO `product_items` VALUES (63, 'product-items/3h6Btgp7Xv7owudKkRsBSlGgWUhraaJtFvLtmdS0.png', '11-M-  Purple', 470000, 11, 100, NULL, '2023-06-20 14:47:36', NULL);
INSERT INTO `product_items` VALUES (64, 'product-items/caI93w4PhMgce1ChQlCBhRBdDazhCsFgdbju1JhA.png', '11-L-White', 470000, 11, 100, NULL, '2023-06-20 14:47:41', NULL);
INSERT INTO `product_items` VALUES (65, 'product-items/3qISnrdA8kDU6BoV61npa50cUADsPc5gnqPRIPdO.png', '11-L-  Purple', 470000, 11, 100, NULL, '2023-06-20 14:47:48', NULL);
INSERT INTO `product_items` VALUES (66, 'product-items/72uchN2mOZVbXBuWzx3AiC8dZgzw36u6p1GA1cGe.png', '12-S- Mix', 500000, 12, 100, NULL, '2023-06-20 14:55:03', NULL);
INSERT INTO `product_items` VALUES (67, 'product-items/sy9GJERJQfbFXMj7mgdxieGIMDjY72ei7xTTQBTs.png', '12-M- Mix', 500000, 12, 100, NULL, '2023-06-20 14:55:08', NULL);
INSERT INTO `product_items` VALUES (68, 'product-items/oR3ULUiJZYALUY1p5WRXrJdqI32fgy3fYhg7PTb3.png', '12-L- Mix', 500000, 12, 100, NULL, '2023-06-20 14:55:14', NULL);
INSERT INTO `product_items` VALUES (69, 'product-items/YB39YmLrH5Oq7mFcR4qezLoNr9BJFLnbqIjL01a3.png', '13-30-Blue', 360000, 13, 200, NULL, '2023-06-20 15:03:24', NULL);
INSERT INTO `product_items` VALUES (70, 'product-items/vFH6Kn6ndHw8LhcaOwgrPo8etcBQ6gqasD8JCOWu.png', '13-31-Blue', 360000, 13, 200, NULL, '2023-06-20 15:03:28', NULL);
INSERT INTO `product_items` VALUES (71, 'product-items/zuedjW6CFLPP8vvPGBchMjuD4lydWQtzWh7mHxOX.png', '13-32-Blue', 360000, 13, 200, NULL, '2023-06-20 15:03:33', NULL);
INSERT INTO `product_items` VALUES (72, 'product-items/0BM7PgCV3zhP4UTTYTmHNCc8UrRAfrLPdm0rrnyD.png', '14-30- Brown', 320000, 14, 200, NULL, '2023-06-20 15:08:00', NULL);
INSERT INTO `product_items` VALUES (73, 'product-items/0coKvSn24MQFL5mrFGOyIr5lMF1xgQdEFC05Cf8f.png', '14-31- Brown', 320000, 14, 200, NULL, '2023-06-20 15:08:05', NULL);
INSERT INTO `product_items` VALUES (74, 'product-items/sJyXz0kIMaPCnbaoGcUJuOHc6ZN8zDqoqT0UJLPW.png', '14-32- Brown', 320000, 14, 200, NULL, '2023-06-20 15:08:10', NULL);

-- ----------------------------
-- Table structure for product_stocks
-- ----------------------------
DROP TABLE IF EXISTS `product_stocks`;
CREATE TABLE `product_stocks`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `total_stock` int(0) NOT NULL,
  `unit_price` int(0) NOT NULL,
  `total_price` int(0) NOT NULL,
  `product_item_id` bigint(0) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_stocks_product_item_id_foreign`(`product_item_id`) USING BTREE,
  CONSTRAINT `product_stocks_product_item_id_foreign` FOREIGN KEY (`product_item_id`) REFERENCES `product_items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_stocks
-- ----------------------------
INSERT INTO `product_stocks` VALUES (7, 100, 300000, 30000000, 6, '2023-06-17 14:29:43', '2023-06-17 14:29:43', NULL);
INSERT INTO `product_stocks` VALUES (8, 100, 400000, 40000000, 7, '2023-06-17 14:31:20', '2023-06-17 14:31:20', NULL);
INSERT INTO `product_stocks` VALUES (9, 100, 400000, 40000000, 8, '2023-06-17 14:31:27', '2023-06-17 14:31:27', NULL);
INSERT INTO `product_stocks` VALUES (10, 100, 400000, 40000000, 9, '2023-06-17 14:31:38', '2023-06-17 14:31:38', NULL);
INSERT INTO `product_stocks` VALUES (11, 100, 150000, 15000000, 10, '2023-06-18 07:41:51', '2023-06-18 07:41:51', NULL);
INSERT INTO `product_stocks` VALUES (12, 100, 1500000, 150000000, 10, '2023-06-18 07:42:35', '2023-06-18 07:42:35', NULL);
INSERT INTO `product_stocks` VALUES (13, 100, 300000, 30000000, 16, '2023-06-20 13:41:33', '2023-06-20 13:41:33', NULL);
INSERT INTO `product_stocks` VALUES (14, 100, 300000, 30000000, 17, '2023-06-20 13:41:48', '2023-06-20 13:41:48', NULL);
INSERT INTO `product_stocks` VALUES (15, 200, 400000, 80000000, 24, '2023-06-20 13:53:20', '2023-06-20 13:53:20', NULL);
INSERT INTO `product_stocks` VALUES (16, 200, 400000, 80000000, 25, '2023-06-20 13:53:26', '2023-06-20 13:53:26', NULL);
INSERT INTO `product_stocks` VALUES (17, 200, 400000, 80000000, 26, '2023-06-20 13:53:33', '2023-06-20 13:53:33', NULL);
INSERT INTO `product_stocks` VALUES (18, 200, 400000, 80000000, 27, '2023-06-20 13:53:40', '2023-06-20 13:53:40', NULL);
INSERT INTO `product_stocks` VALUES (19, 200, 400000, 80000000, 28, '2023-06-20 13:53:46', '2023-06-20 13:53:46', NULL);
INSERT INTO `product_stocks` VALUES (20, 200, 400000, 80000000, 29, '2023-06-20 13:53:52', '2023-06-20 13:53:52', NULL);
INSERT INTO `product_stocks` VALUES (21, 300, 420000, 126000000, 30, '2023-06-20 14:10:14', '2023-06-20 14:10:14', NULL);
INSERT INTO `product_stocks` VALUES (22, 300, 420000, 126000000, 31, '2023-06-20 14:10:20', '2023-06-20 14:10:20', NULL);
INSERT INTO `product_stocks` VALUES (23, 300, 420000, 126000000, 32, '2023-06-20 14:10:25', '2023-06-20 14:10:25', NULL);
INSERT INTO `product_stocks` VALUES (24, 300, 420000, 126000000, 33, '2023-06-20 14:10:31', '2023-06-20 14:10:31', NULL);
INSERT INTO `product_stocks` VALUES (25, 300, 420000, 126000000, 34, '2023-06-20 14:10:36', '2023-06-20 14:10:36', NULL);
INSERT INTO `product_stocks` VALUES (26, 300, 420000, 126000000, 35, '2023-06-20 14:10:43', '2023-06-20 14:10:43', NULL);
INSERT INTO `product_stocks` VALUES (27, 100, 300000, 30000000, 36, '2023-06-20 14:17:04', '2023-06-20 14:17:04', NULL);
INSERT INTO `product_stocks` VALUES (28, 100, 300000, 30000000, 37, '2023-06-20 14:17:09', '2023-06-20 14:17:09', NULL);
INSERT INTO `product_stocks` VALUES (29, 100, 300000, 30000000, 38, '2023-06-20 14:17:15', '2023-06-20 14:17:15', NULL);
INSERT INTO `product_stocks` VALUES (30, 100, 300000, 30000000, 39, '2023-06-20 14:17:20', '2023-06-20 14:17:20', NULL);
INSERT INTO `product_stocks` VALUES (31, 100, 300000, 30000000, 40, '2023-06-20 14:17:26', '2023-06-20 14:17:26', NULL);
INSERT INTO `product_stocks` VALUES (32, 100, 300000, 30000000, 41, '2023-06-20 14:17:32', '2023-06-20 14:17:32', NULL);
INSERT INTO `product_stocks` VALUES (33, 100, 120000, 12000000, 42, '2023-06-20 14:22:29', '2023-06-20 14:22:29', NULL);
INSERT INTO `product_stocks` VALUES (34, 100, 120000, 12000000, 43, '2023-06-20 14:22:35', '2023-06-20 14:22:35', NULL);
INSERT INTO `product_stocks` VALUES (35, 100, 120000, 12000000, 44, '2023-06-20 14:22:41', '2023-06-20 14:22:41', NULL);
INSERT INTO `product_stocks` VALUES (36, 100, 120000, 12000000, 45, '2023-06-20 14:22:53', '2023-06-20 14:22:53', NULL);
INSERT INTO `product_stocks` VALUES (37, 100, 120000, 12000000, 46, '2023-06-20 14:23:00', '2023-06-20 14:23:00', NULL);
INSERT INTO `product_stocks` VALUES (38, 100, 120000, 12000000, 47, '2023-06-20 14:23:07', '2023-06-20 14:23:07', NULL);
INSERT INTO `product_stocks` VALUES (39, 150, 520000, 78000000, 48, '2023-06-20 14:29:48', '2023-06-20 14:29:48', NULL);
INSERT INTO `product_stocks` VALUES (40, 150, 520000, 78000000, 49, '2023-06-20 14:29:53', '2023-06-20 14:29:53', NULL);
INSERT INTO `product_stocks` VALUES (41, 150, 520000, 78000000, 50, '2023-06-20 14:30:00', '2023-06-20 14:30:00', NULL);
INSERT INTO `product_stocks` VALUES (42, 150, 520000, 78000000, 51, '2023-06-20 14:30:06', '2023-06-20 14:30:06', NULL);
INSERT INTO `product_stocks` VALUES (43, 150, 520000, 78000000, 52, '2023-06-20 14:30:15', '2023-06-20 14:30:15', NULL);
INSERT INTO `product_stocks` VALUES (44, 150, 520000, 78000000, 53, '2023-06-20 14:30:25', '2023-06-20 14:30:25', NULL);
INSERT INTO `product_stocks` VALUES (45, 200, 330000, 66000000, 54, '2023-06-20 14:34:01', '2023-06-20 14:34:01', NULL);
INSERT INTO `product_stocks` VALUES (46, 200, 330000, 66000000, 55, '2023-06-20 14:34:21', '2023-06-20 14:34:21', NULL);
INSERT INTO `product_stocks` VALUES (47, 200, 330000, 66000000, 56, '2023-06-20 14:34:26', '2023-06-20 14:34:26', NULL);
INSERT INTO `product_stocks` VALUES (48, 200, 330000, 66000000, 57, '2023-06-20 14:34:32', '2023-06-20 14:34:32', NULL);
INSERT INTO `product_stocks` VALUES (49, 200, 330000, 66000000, 58, '2023-06-20 14:34:38', '2023-06-20 14:34:38', NULL);
INSERT INTO `product_stocks` VALUES (50, 200, 330000, 66000000, 59, '2023-06-20 14:34:44', '2023-06-20 14:34:44', NULL);
INSERT INTO `product_stocks` VALUES (51, 100, 400000, 40000000, 60, '2023-06-20 14:47:02', '2023-06-20 14:47:02', NULL);
INSERT INTO `product_stocks` VALUES (52, 100, 400000, 40000000, 61, '2023-06-20 14:47:25', '2023-06-20 14:47:25', NULL);
INSERT INTO `product_stocks` VALUES (53, 100, 400000, 40000000, 62, '2023-06-20 14:47:30', '2023-06-20 14:47:30', NULL);
INSERT INTO `product_stocks` VALUES (54, 100, 400000, 40000000, 63, '2023-06-20 14:47:36', '2023-06-20 14:47:36', NULL);
INSERT INTO `product_stocks` VALUES (55, 100, 400000, 40000000, 64, '2023-06-20 14:47:41', '2023-06-20 14:47:41', NULL);
INSERT INTO `product_stocks` VALUES (56, 100, 400000, 40000000, 65, '2023-06-20 14:47:48', '2023-06-20 14:47:48', NULL);
INSERT INTO `product_stocks` VALUES (57, 100, 400000, 40000000, 66, '2023-06-20 14:55:03', '2023-06-20 14:55:03', NULL);
INSERT INTO `product_stocks` VALUES (58, 100, 400000, 40000000, 67, '2023-06-20 14:55:08', '2023-06-20 14:55:08', NULL);
INSERT INTO `product_stocks` VALUES (59, 100, 400000, 40000000, 68, '2023-06-20 14:55:14', '2023-06-20 14:55:14', NULL);
INSERT INTO `product_stocks` VALUES (60, 200, 280000, 56000000, 69, '2023-06-20 15:03:24', '2023-06-20 15:03:24', NULL);
INSERT INTO `product_stocks` VALUES (61, 200, 280000, 56000000, 70, '2023-06-20 15:03:28', '2023-06-20 15:03:28', NULL);
INSERT INTO `product_stocks` VALUES (62, 200, 280000, 56000000, 71, '2023-06-20 15:03:33', '2023-06-20 15:03:33', NULL);
INSERT INTO `product_stocks` VALUES (63, 200, 200000, 40000000, 72, '2023-06-20 15:08:00', '2023-06-20 15:08:00', NULL);
INSERT INTO `product_stocks` VALUES (64, 200, 200000, 40000000, 73, '2023-06-20 15:08:05', '2023-06-20 15:08:05', NULL);
INSERT INTO `product_stocks` VALUES (65, 200, 200000, 40000000, 74, '2023-06-20 15:08:10', '2023-06-20 15:08:10', NULL);

-- ----------------------------
-- Table structure for product_variation_option_values
-- ----------------------------
DROP TABLE IF EXISTS `product_variation_option_values`;
CREATE TABLE `product_variation_option_values`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_variation_id` bigint(0) UNSIGNED NOT NULL,
  `variation_option_id` int(0) NOT NULL,
  `variation_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_variation_option_values_product_variation_id_foreign`(`product_variation_id`) USING BTREE,
  CONSTRAINT `product_variation_option_values_product_variation_id_foreign` FOREIGN KEY (`product_variation_id`) REFERENCES `product_variation_options` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 79 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_variation_option_values
-- ----------------------------
INSERT INTO `product_variation_option_values` VALUES (7, 3, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (8, 3, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (9, 3, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (10, 4, 13, 'Yellow', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (11, 5, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (12, 5, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (13, 5, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (15, 7, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (16, 7, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (17, 7, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (18, 8, 10, 'Blue', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (19, 8, 11, 'Red', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (26, 10, 20, 'White', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (27, 10, 22, 'Black', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (28, 11, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (29, 11, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (30, 11, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (31, 12, 20, 'White', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (32, 12, 22, 'Black', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (33, 13, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (34, 13, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (35, 13, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (38, 15, 23, 'Blue', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (39, 15, 28, 'Orange', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (40, 16, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (41, 16, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (42, 16, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (43, 17, 25, 'White', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (44, 17, 27, 'Black', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (45, 18, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (46, 18, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (47, 18, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (48, 19, 25, 'White', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (49, 19, 27, 'Black', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (50, 20, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (51, 20, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (52, 20, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (53, 21, 23, 'Blue', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (54, 21, 25, 'White', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (55, 22, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (56, 22, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (57, 22, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (58, 23, 25, 'White', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (59, 23, 27, 'Black', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (60, 24, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (61, 24, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (62, 24, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (65, 26, 45, 'White', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (66, 26, 49, '  Purple', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (67, 27, 1, 'S', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (68, 27, 2, 'M', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (69, 27, 3, 'L', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (70, 28, 57, ' Mix', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (71, 29, 66, '30', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (72, 29, 67, '31', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (73, 29, 68, '32', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (74, 30, 50, 'Blue', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (75, 31, 66, '30', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (76, 31, 67, '31', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (77, 31, 68, '32', NULL, NULL, NULL);
INSERT INTO `product_variation_option_values` VALUES (78, 32, 76, ' Brown', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for product_variation_options
-- ----------------------------
DROP TABLE IF EXISTS `product_variation_options`;
CREATE TABLE `product_variation_options`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` bigint(0) UNSIGNED NOT NULL,
  `variation_id` int(0) NOT NULL,
  `variation_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_variation_options_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `product_variation_options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_variation_options
-- ----------------------------
INSERT INTO `product_variation_options` VALUES (3, 2, 1, 'Size', '2023-06-17 14:26:24', '2023-06-17 14:26:24', NULL);
INSERT INTO `product_variation_options` VALUES (4, 2, 2, 'Color', '2023-06-17 14:28:02', '2023-06-17 14:28:02', NULL);
INSERT INTO `product_variation_options` VALUES (5, 3, 1, 'Size', '2023-06-17 14:30:20', '2023-06-17 14:30:20', NULL);
INSERT INTO `product_variation_options` VALUES (7, 4, 1, 'Size', '2023-06-18 07:39:20', '2023-06-18 07:39:20', NULL);
INSERT INTO `product_variation_options` VALUES (8, 4, 2, 'Color', '2023-06-18 07:39:37', '2023-06-18 07:39:37', NULL);
INSERT INTO `product_variation_options` VALUES (10, 3, 2, 'Color', '2023-06-20 13:47:34', '2023-06-20 13:47:34', NULL);
INSERT INTO `product_variation_options` VALUES (11, 5, 1, 'Size', '2023-06-20 13:52:11', '2023-06-20 13:52:11', NULL);
INSERT INTO `product_variation_options` VALUES (12, 5, 2, 'Color', '2023-06-20 13:52:17', '2023-06-20 13:52:17', NULL);
INSERT INTO `product_variation_options` VALUES (13, 6, 1, 'Size', '2023-06-20 14:07:03', '2023-06-20 14:07:03', NULL);
INSERT INTO `product_variation_options` VALUES (15, 6, 2, 'Color', '2023-06-20 14:08:37', '2023-06-20 14:08:37', NULL);
INSERT INTO `product_variation_options` VALUES (16, 7, 1, 'Size', '2023-06-20 14:15:07', '2023-06-20 14:15:07', NULL);
INSERT INTO `product_variation_options` VALUES (17, 7, 2, 'Color', '2023-06-20 14:15:16', '2023-06-20 14:15:16', NULL);
INSERT INTO `product_variation_options` VALUES (18, 8, 1, 'Size', '2023-06-20 14:21:23', '2023-06-20 14:21:23', NULL);
INSERT INTO `product_variation_options` VALUES (19, 8, 2, 'Color', '2023-06-20 14:21:30', '2023-06-20 14:21:30', NULL);
INSERT INTO `product_variation_options` VALUES (20, 9, 1, 'Size', '2023-06-20 14:28:30', '2023-06-20 14:28:30', NULL);
INSERT INTO `product_variation_options` VALUES (21, 9, 2, 'Color', '2023-06-20 14:28:40', '2023-06-20 14:28:40', NULL);
INSERT INTO `product_variation_options` VALUES (22, 10, 1, 'Size', '2023-06-20 14:32:54', '2023-06-20 14:32:54', NULL);
INSERT INTO `product_variation_options` VALUES (23, 10, 2, 'Color', '2023-06-20 14:33:00', '2023-06-20 14:33:00', NULL);
INSERT INTO `product_variation_options` VALUES (24, 11, 1, 'Size', '2023-06-20 14:39:20', '2023-06-20 14:39:20', NULL);
INSERT INTO `product_variation_options` VALUES (26, 11, 2, 'Color', '2023-06-20 14:43:21', '2023-06-20 14:43:21', NULL);
INSERT INTO `product_variation_options` VALUES (27, 12, 1, 'Size', '2023-06-20 14:53:11', '2023-06-20 14:53:11', NULL);
INSERT INTO `product_variation_options` VALUES (28, 12, 2, 'Color', '2023-06-20 14:53:16', '2023-06-20 14:53:16', NULL);
INSERT INTO `product_variation_options` VALUES (29, 13, 1, 'Size', '2023-06-20 15:02:32', '2023-06-20 15:02:32', NULL);
INSERT INTO `product_variation_options` VALUES (30, 13, 2, 'Color', '2023-06-20 15:02:39', '2023-06-20 15:02:39', NULL);
INSERT INTO `product_variation_options` VALUES (31, 14, 1, 'Size', '2023-06-20 15:06:33', '2023-06-20 15:06:33', NULL);
INSERT INTO `product_variation_options` VALUES (32, 14, 2, 'Color', '2023-06-20 15:07:06', '2023-06-20 15:07:06', NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(0) UNSIGNED NOT NULL,
  `sub_category_id` bigint(0) UNSIGNED NOT NULL,
  `promotion_id` bigint(0) UNSIGNED NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `products_category_id_foreign`(`category_id`) USING BTREE,
  INDEX `products_sub_category_id_foreign`(`sub_category_id`) USING BTREE,
  INDEX `products_promotion_id_foreign`(`promotion_id`) USING BTREE,
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `products_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `promotions` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `products_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (2, 'Áo Polo trơn - APV231321', '<div><ul><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul></div>', 'products/I1Zkm1B4kolTywZm5xticD53i9nFPSy3hz7zGVfU.png', 10, 29, NULL, '2023-06-17 13:53:24', '2023-06-20 13:37:17', NULL);
INSERT INTO `products` VALUES (3, 'ÁO POLO trơn - APV231362', '<ul><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul>', 'products/zpryJi6B0dpAy6xkXf3wkd48xW0lcrLtWqgFcdfs.png', 10, 29, NULL, '2023-06-17 14:20:43', '2023-06-20 13:37:37', NULL);
INSERT INTO `products` VALUES (4, 'váy 1-m123', '<p style=\"margin-right: 0px; margin-left: 0px; padding: 0px; font-family: Quicksand, sans-serif; font-size: 14px;\"><b style=\"\"><font color=\"#000000\" style=\"background-color: rgb(255, 255, 0);\">Áo Polo trơn TORANO cổ bẻ tay ngắn trơn, bo kẻ nhiều màu ESTP038 chính là item hoàn hảo dễ mặc dễ phối đồ cho cả nam và nữ. Khám phá ngay!!!</font></b></p><div><br></div>', 'products/NxZJsF0tOW0CJKxXfArEZkQjqJWbcf3w2iLtNuS2.jpg', 12, 37, NULL, '2023-06-18 07:38:43', '2023-06-18 07:56:05', '2023-06-18 07:56:05');
INSERT INTO `products` VALUES (5, 'Áo Polo trơn bo kẻ cổ ESTP067', '<ul><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul>', 'products/G6IQstInkZcHc7TC0W4nzYYWKgOBCwuRK6yX7YiP.png', 10, 29, NULL, '2023-06-20 13:51:58', '2023-06-20 14:11:35', NULL);
INSERT INTO `products` VALUES (6, 'Áo polo active Torano TP104', '<ul><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul>', 'products/wL9IhPTy8aUML8lu7GdSyogYxcZjBHgM8a7e3zcp.png', 10, 29, NULL, '2023-06-20 14:06:53', '2023-06-20 14:11:41', NULL);
INSERT INTO `products` VALUES (7, 'Áo Thun in hình Barcode - ESTS004', '<div style=\"text-align: left;\"><ul><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul></div>', 'products/W372cyBwSpF0qra61xO036nim81bmgHfIu7F5ZYL.png', 10, 30, NULL, '2023-06-20 14:14:51', '2023-06-20 14:28:02', NULL);
INSERT INTO `products` VALUES (8, 'Áo thun Tanktop họa tiết in Fokus on the good things ESTA008', '<div style=\"text-align: left;\"><ul><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul></div>', 'products/lCRbzj0Cp1K3WDnrniXKHMrWpBxS8NcOKaOqpE6O.png', 10, 30, NULL, '2023-06-20 14:19:49', '2023-06-20 14:28:10', NULL);
INSERT INTO `products` VALUES (9, 'Sơ mi ngắn tay trơn Oxford thêu logo DSTB605', '<h1 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-size: 26px; color: rgb(51, 51, 51); font-family: Quicksand, sans-serif;\"><ul style=\"color: rgb(33, 37, 41); font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul></h1>', 'products/6ux8E9dbc4v0i38TmM571hjhq4oSMS2zkCADO6QR.png', 10, 31, NULL, '2023-06-20 14:27:46', '2023-06-20 14:28:17', NULL);
INSERT INTO `products` VALUES (10, 'Sơ mi ngắn tay đũi 5.ESTB026', '<h1 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-weight: 700; font-size: 26px; color: rgb(51, 51, 51); font-family: Quicksand, sans-serif;\"><ul style=\"font-weight: 500; color: rgb(33, 37, 41); font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul></h1>', 'products/Qt8kGJUkWW3HAqS9Iz21z0PJ1MjFOPRztgwRSTdc.png', 10, 31, NULL, '2023-06-20 14:32:44', '2023-06-20 14:35:06', NULL);
INSERT INTO `products` VALUES (11, 'Áo sơ mi dài tay trơn Bamboo DATB613', '<h1 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-weight: 700; font-size: 26px; color: rgb(51, 51, 51); font-family: Quicksand, sans-serif;\"><ul style=\"font-weight: 500; color: rgb(33, 37, 41); font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul></h1>', 'products/1iXhS7bHy663NGMkzxYlt05NGBlCnLf9StKFD22E.png', 10, 32, NULL, '2023-06-20 14:39:11', '2023-06-20 14:43:07', NULL);
INSERT INTO `products` VALUES (12, 'Sơ mi dài tay kẻ Checkerboard 2.DATB416', '<h1 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-weight: 700; font-size: 26px; color: rgb(51, 51, 51); font-family: Quicksand, sans-serif;\"><ul style=\"font-weight: 500; color: rgb(33, 37, 41); font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px;\"><li style=\"text-align: left;\">Áo Polo kiểu dáng body fit, tôn dáng người mặc.</li><li style=\"text-align: left;\">Màu sắc trung tính, dễ phối đồ.</li><li style=\"text-align: left;\">Chất liệu: CVC Spandex</li></ul></h1>', 'products/NyNM4f0rp829lIe3xvzIeY7VgclqdumrD5d5c3KP.png', 10, 32, NULL, '2023-06-20 14:52:59', '2023-06-20 14:54:18', NULL);
INSERT INTO `products` VALUES (13, 'QUẦN KAKI - QKSL231302', '<h4 class=\"\"></h4><p></p><p></p><ul><ul></ul></ul><p></p><h3 class=\"\"></h3><h4 class=\"\"></h4><ul><li style=\"text-align: left;\"><span style=\"color: rgb(50, 50, 50); font-family: Arial; font-size: 12px;\">Chất liệu mềm mại thoáng mát, đứng form dáng</span></li><li style=\"text-align: left;\"><span style=\"color: rgb(50, 50, 50); font-family: Arial; font-size: 12px;\">Tone màu trẻ trung, hiện đại, dễ phối đồ.</span></li><li><div style=\"text-align: left;\"><span style=\"color: rgb(50, 50, 50); font-family: Arial; font-size: 12px;\">Thiết kế thanh lịch, thích hợp diện mặc khi tới công sở, dự sự kiện.</span></div><div style=\"text-align: left;\"><font color=\"#323232\" face=\"Arial\"><span style=\"font-size: 12px;\"><br></span></font></div><br></li></ul><p><span style=\"font-family: Arial;\">﻿</span></p><h4 class=\"\"><span style=\"font-family: Arial;\">﻿</span></h4><p></p><ul><ul></ul></ul><p></p>', 'products/uqKl1r5acufp3D2uayIrXrM0zCS5vChwRubTwdLr.png', 11, 33, NULL, '2023-06-20 15:02:16', '2023-06-20 15:13:24', NULL);
INSERT INTO `products` VALUES (14, 'QUẦN KHAKI - QKSL221140', '<h1 class=\"page-title\" style=\"font-family: SVN-Gotham, sans-serif; font-weight: 700; line-height: 21px; font-size: 14px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; color: rgb(50, 50, 50); text-transform: uppercase; order: 2; width: calc(100% - 50px);\"><ul style=\"color: rgb(33, 37, 41); font-family: &quot;Source Sans Pro&quot;, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; font-size: 16px; font-weight: 400; text-transform: none;\"><li style=\"text-align: left;\"><span style=\"color: rgb(50, 50, 50); font-family: Arial; font-size: 12px;\">Chất liệu mềm mại thoáng mát, đứng form dáng</span></li><li style=\"text-align: left;\"><span style=\"color: rgb(50, 50, 50); font-family: Arial; font-size: 12px;\">Tone màu trẻ trung, hiện đại, dễ phối đồ.</span></li><li><span style=\"color: rgb(50, 50, 50); font-family: Arial; font-size: 12px;\">Thiết kế thanh lịch, thích hợp diện mặc khi tới công sở, dự sự kiện.</span></li></ul></h1>', 'products/reKEBuOQ0Z7yyX66h8a9evgwmR3URMzUQuyMl6mm.png', 11, 33, NULL, '2023-06-20 15:06:22', '2023-06-20 15:13:55', NULL);

-- ----------------------------
-- Table structure for promotions
-- ----------------------------
DROP TABLE IF EXISTS `promotions`;
CREATE TABLE `promotions`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount_rate` int(0) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for provinces
-- ----------------------------
DROP TABLE IF EXISTS `provinces`;
CREATE TABLE `provinces`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sub_categories
-- ----------------------------
DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE `sub_categories`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(0) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `sub_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sub_categories
-- ----------------------------
INSERT INTO `sub_categories` VALUES (29, 'Áo polo', 10, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (30, 'Áo thun', 10, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (31, ' Sơ mi ngắn tay', 10, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (32, ' Sơ mi dài tay', 10, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (33, 'Quần kaki', 11, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (34, ' Quần âu', 11, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (35, ' Quần đũi', 11, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (36, ' Quần gió', 11, NULL, NULL, NULL);
INSERT INTO `sub_categories` VALUES (37, 'váy 1', 12, NULL, '2023-06-18 07:56:13', '2023-06-18 07:56:13');
INSERT INTO `sub_categories` VALUES (38, ' váy 2', 12, NULL, '2023-06-18 07:56:16', '2023-06-18 07:56:16');
INSERT INTO `sub_categories` VALUES (39, ' váy 3', 12, NULL, '2023-06-18 07:56:19', '2023-06-18 07:56:19');

-- ----------------------------
-- Table structure for user_reviews
-- ----------------------------
DROP TABLE IF EXISTS `user_reviews`;
CREATE TABLE `user_reviews`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(0) UNSIGNED NOT NULL,
  `order_detail_id` bigint(0) UNSIGNED NOT NULL,
  `rating_value` int(0) NOT NULL,
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_reviews_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `user_reviews_order_detail_id_foreign`(`order_detail_id`) USING BTREE,
  CONSTRAINT `user_reviews_order_detail_id_foreign` FOREIGN KEY (`order_detail_id`) REFERENCES `order_details` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(0) NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` int(0) NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `provider_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (7, 'dangxuanloc96@gmail.com', 'Lộc Đặng', 0, NULL, NULL, 1, NULL, '$2y$10$yO5qOX27fODncpWNKpS4IeDX2gl6GT2Mj18e0Vvz5dDlrLTbdykay', NULL, '2023-06-18 08:01:12', '2023-06-18 08:01:12', NULL, 'google', '112686078018265717416');

-- ----------------------------
-- Table structure for variation_options
-- ----------------------------
DROP TABLE IF EXISTS `variation_options`;
CREATE TABLE `variation_options`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variation_id` bigint(0) UNSIGNED NOT NULL,
  `variation_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `variation_options_variation_id_foreign`(`variation_id`) USING BTREE,
  CONSTRAINT `variation_options_variation_id_foreign` FOREIGN KEY (`variation_id`) REFERENCES `variations` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of variation_options
-- ----------------------------
INSERT INTO `variation_options` VALUES (61, 1, 'S', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (62, 1, 'M', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (63, 1, 'L', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (64, 1, 'XL', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (65, 1, '29', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (66, 1, '30', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (67, 1, '31', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (68, 1, '32', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (69, 2, 'Blue', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (70, 2, 'Red', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (71, 2, 'White', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (72, 2, 'Yellow', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (73, 2, 'Black', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (74, 2, 'Orange', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (75, 2, '  Purple', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (76, 2, ' Brown', NULL, NULL, NULL);
INSERT INTO `variation_options` VALUES (77, 2, 'Mix', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for variations
-- ----------------------------
DROP TABLE IF EXISTS `variations`;
CREATE TABLE `variations`  (
  `id` bigint(0) UNSIGNED NOT NULL AUTO_INCREMENT,
  `variation_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of variations
-- ----------------------------
INSERT INTO `variations` VALUES (1, 'Size', '2023-06-17 08:20:06', '2023-06-20 14:58:55', NULL);
INSERT INTO `variations` VALUES (2, 'Color', '2023-06-17 08:20:29', '2023-06-20 15:06:53', NULL);

SET FOREIGN_KEY_CHECKS = 1;
